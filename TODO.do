Use popen lib call to open an exec'd prog's output to read back in...

Updated: 19990618

Need to stabilize a basic UI structure so that all developers have a
code and UI structure to work within.  The UI mockups below should be
considered a *starting* point but other alternatives that might work
as well or better will be considered.


Updated: 19990609

Start of work on project property dialog/settings

Cases:

1) Use Auto[conf,make]

Track list of files to be compiled in automake.am

2) Makefile (compiled to executable)

- Makefile is generated/updated automatically
- All you need are 2 command strings (Compile to .o and link command) plus
  the list of source files and libraries to link

|----------Project Manager----------|
|  ____________  _________          |
| |Source files||Libraries|         |
| |            +------------------  |
| |                              |  |
| |                              |  |
| |                              |  |
| |                              |  |
| |                              |  |
| --------------------------------  |
|                                   |
|  NEW  ADD   REMOVE   PROPERTIES   |
|                                   |
|-----------------------------------|
s
|---------Project Properties--------|
|                                   |
| Compile command:                  |
| ##############################    |
|                                   |
| Link command:                     |
| ##############################    |
|                                   |
| Run command args:                 |
| ##############################    |
|                                   |
|                                   |
|   OK      CANCEL       HELP       |
|                                   |
|-----------------------------------|


3) Makefile (interpreted environment like Java or PERL)

- Makefile is generated/updated automatically
- Need a compile command, a run command, a list of sources to compile, and
  the main module to run with the interpreter
- Some of these choices are optional


NOTE: Various file types will be built in with reasonable project
defaults automatically supplied.

