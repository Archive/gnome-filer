/*
 * libwatchpoint: data watchpoint manager
 *
 * Copyright 2000  by Anthony Taylor <tony@searhc.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>
#include "watchpoint.h"

/*
 * NOTE: This library automatically sets a watchpoint "new"
 * that gets triggered when the first data property is set on
 * the object.
*/

static void data_exec_watchpoint (GHashTable *table,
				  GtkObject  *object,
				  gchar      *key,
				  gpointer    old_data);

static void data_name_change (GtkObject  *object,
			      gchar      *key,
			      gpointer    blah);

static gpointer data_set_real (GtkObject *object,
			       gchar *key,
			       gpointer data,
			       gboolean exec);

static void data_get_data_name (gpointer key, 
				gpointer data,
				GList   *list);

static void data_get_data_name (gpointer key, 
				gpointer data,
				GList   *list);

static GHashTable *watch_keys  = NULL;
static GHashTable *all_objects = NULL;


/*
 * NAME:    data_register_widget
 *
 * ARGS:    GtkWidget * -- the new widget to register
 *
 * RETURNS: NA
 *
 * PURPOSE: Creates the data structures necessary for data
 *          management.
 *
 * NOTES:   Fakes a "new" watchpoint call
*/

void
data_register_widget (GtkObject *object)
{
	GHashTable *hash;
	
	hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
	if (object && !hash)
	{
		hash = g_hash_table_new (g_str_hash, g_str_equal);
		gtk_object_set_data  (object, "WatchData", hash);
		data_set (object, "new", NULL);
		data_set_watchpoint  (object, "name", data_name_change, NULL);
	}
}


/*
 * NAME:    data_set_watchpoint
 *
 * ARGS:    GtkObject * -- the object to watch
 *          gchar *     -- the key within the object to watch
 *          gpointer    -- the function to call
 *          gpointer    -- user data (SEE $(SRCDIR)/docs/watchpoint.txt)
 *
 * RETURNS: NA
 *
 * PURPOSE: Sets up a request queue for each object/key.  When the
 *          specified key in the specified object is changed, the
 *          specified function will get called.  This is similar to,
 *          but less powerful than, Gtk+'s signal method.
 *
 * NOTES:   If the (GtkObject *) argument is NULL, then this watchpoint
 *          will be set for *all* objects, whether they exist yet or
 *          not, which is probably not what you want.  This is mostly 
 *          useful for the major IDE plugins.
 *
 *          The global list of watchpoints is stored in "watch_keys",
 *          which is defined at the top of this file.
 *
 *          Because watchpoints are generic by nature, the function
 *          callback is very simple, and is prototyped like this:
 *
 *             static void watchpoint_func (GtkObject *foo,
 *                                          gchar *key,
 *                                          gpointer old_value);
 *
 *          The data for the key can be retrieved using
 *          gtk_object_get_data(), or data_get(), which is merely a
 *          wrapper function for gtk_object_get_data().
 *
 *          Callbacks are stored in a linked list, which is stored in 
 *          the key name.
*/

void
data_set_watchpoint (GtkObject *object, gchar *key, 
		     gpointer cb, gpointer user_data)
{
	GHashTable  *hash;
	WatchFunc   *wfunc;
	WatchData   *w_data = NULL;
	GSList      *st;
	int          i, length;
	
	if (!object)
	{
		if (!watch_keys)
		{
			watch_keys = g_hash_table_new (g_str_hash, g_str_equal);
		}
		hash = watch_keys;
	}
	else
	{
		hash = (GHashTable *) gtk_object_get_data (object,
							   "WatchData");
		
		if (!hash)
		{
			data_register_widget (object);
			hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
		}
	}
	
	w_data = (WatchData *) g_hash_table_lookup (hash, key);
	
	/*
	 *  IFF the list doesn't exist yet, create it.
	 */
	if (!w_data)
	{
		w_data = g_malloc (sizeof (WatchData));
		w_data->cblist = g_slist_alloc ();
		g_hash_table_insert (hash, key, (gpointer) w_data);
		w_data->lock = FALSE;
		w_data->data = NULL;
		w_data->function_body = NULL;
	}
	
	/* 
	 * This bit just verifies that the callback is unique
	 */
	length = g_slist_length (w_data->cblist);
	for (i=0; i<length; i++)
	{
		st = g_slist_nth (w_data->cblist, i);
		wfunc = (WatchFunc *) st->data;
		if (wfunc && wfunc->callback == cb && wfunc->cb_data == user_data)
		{
			break;
		}
	}
	if (i == length)
	{
		wfunc = g_malloc (sizeof (WatchFunc));
		wfunc->callback = cb;
		wfunc->cb_data  = user_data;
		w_data->cblist = g_slist_prepend (w_data->cblist, wfunc);
	}
}


/*
 * NAME:    data_remove_watchpoint
 *
 * ARGS:    GtkObject * -- the object to ignore
 *          gchar *     -- the key within the object to ignore
 *          void *      -- the function to remove
 *
 * RETURNS: NA
 *
 * PURPOSE: Remove a watchpoint
 *
 * NOTES:   The inverse of data_set_watchpoint
*/

void
data_remove_watchpoint (GtkObject *object, gchar *key, gpointer cb)
{
	GHashTable  *hash;
	WatchData   *w_data;
	WatchFunc   *w_func = NULL;
	GSList      *st = NULL;
	int          i, length;
	
	if (!object)
	{
		if (!watch_keys)
		{
			watch_keys = g_hash_table_new (g_str_hash, g_str_equal);
		}
		hash = watch_keys;
	}
	else
	{
		hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
	}
	w_data = (WatchData *) g_hash_table_lookup (hash, key);
	
	/* This bit finds the callback to remove
	 */
	length = g_slist_length (w_data->cblist);
	
	for (i=0; i<length; i++)
	{
		st = g_slist_nth (w_data->cblist, i);
		w_func = (WatchFunc *) st->data;
		if (w_func->callback == cb)
		{
			break;
		}
	}
	if ((i != length) && w_func && st)
	{
		g_free (w_func);
		w_data->cblist = g_slist_remove_link (w_data->cblist, st);
	}
}


/*
 * NAME:    data_initialize
 *
 * ARGS:    GtkObject * -- the object to set
 *          gchar *     -- the key within the object to set
 *          gpointer    -- the data to associate
 *          DataType    -- the type of data to store
 *
 * RETURNS: The value being replaced.
 *
 * PURPOSE: Associates data with "key," and marks it of type "type"
 *
 * NOTES:   SEE data.h for a list of DataTypes
*/

gpointer
data_initialize (GtkObject *object,
	         gchar *key, 
		 gpointer data,
		 DataType type)
{
	data_set_type (object, key, type);
	return data_set_real (object, key, data, TRUE);
}


/*
 * NAME:    data_set_type
 *
 * ARGS:    GtkObject * -- the object to set
 *          gchar *     -- the key within the object to set
 *          DataType    -- mark this key as type "type"
 *
 * RETURNS:
 *
 * PURPOSE: Simply marks the data in "key" as "type"
 *
 * NOTES:   SEE data.h for a list of DataTypes
*/

void
data_set_type (GtkObject *object, gchar *key, DataType type)
{
	WatchData  *w_data;
	GHashTable *hash;
	
	if (object)
	{
		hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
		
		if (!hash)
		{
			data_register_widget (object);
			hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
		}
		w_data = (WatchData *) g_hash_table_lookup (hash, key);
		if (!w_data)
		{
			w_data = g_malloc (sizeof (WatchData));
			w_data->cblist = g_slist_alloc ();
			g_hash_table_insert (hash, key, (gpointer) w_data);
			w_data->lock = FALSE;
			w_data->data = NULL;
			w_data->function_body = NULL;
		}
		w_data->type = type;
	}
}


/*
 * NAME:    data_get_type
 *
 * ARGS:    GtkObject * -- the object from which to fetch
 *          gchar *     -- the key to fetch within the object
 *
 * RETURNS: The datatype of "key"
 *
 * PURPOSE: Simply returns the datatype
 *
 * NOTES:   SEE data.h for a list of DataTypes
*/

DataType
data_get_type (GtkObject *object, gchar *key)
{
	WatchData  *w_data = NULL;
	GHashTable *hash;
	
	if (object)
	{
		hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
		
		if (!hash)
		{
			data_register_widget (object);
			hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
		}
		w_data = (WatchData *) g_hash_table_lookup (hash, key);
		if (!w_data)
		{
			w_data = g_malloc (sizeof (WatchData));
			w_data->cblist = g_slist_alloc ();
			g_hash_table_insert (hash, key, (gpointer) w_data);
			w_data->lock = FALSE;
			w_data->data = NULL;
			w_data->function_body = NULL;
		}
	}
	return w_data->type;
}


/*
 * NAME:    data_set_no_watchpoints
 *
 * ARGS:    GtkObject * -- the object to set
 *          gchar *     -- the key within the object to set
 *          gpointer    -- the data to associate
 *
 * RETURNS: The value being replaced.
 *
 * PURPOSE: Associates data with key, but does not execute watchpoints
 *
 * NOTES:   
*/

gpointer
data_set_no_watchpoints (GtkObject *object, gchar *key, gpointer data)
{
	return data_set_real (object, key, data, FALSE);
}


/*
 * NAME:    data_set
 *
 * ARGS:    GtkObject * -- the object to set
 *          gchar *     -- the key within the object to set
 *          gpointer    -- the data to associate
 *
 * RETURNS: The value being replaced.
 *
 * PURPOSE: Associates data with key.  Also, calls any watch functions
 *          hooked to that key.
 *
 * NOTES:
*/

gpointer
data_set (GtkObject *object, gchar *key, gpointer data)
{
	return data_set_real (object, key, data, TRUE);
}


/*
 * NAME:    data_set_real
 *
 * ARGS:    GtkObject * -- the object to set
 *          gchar *     -- the key within the object to set
 *          gpointer    -- the data to associate
 *          gboolean    -- a flag indicating whether to execute the
 *                         watchpoints, or not.
 *
 * RETURNS: The value being replaced.
 *
 * PURPOSE: Associates data with key.  Also, calls any watch functions
 *          hooked to that key.
 *
 * NOTES:   This locks the data that is being changed, while the
 *          watchpoint callbacks are executing.  This prevents
 *          race conditions.
*/

static gpointer
data_set_real (GtkObject *object, gchar *key, gpointer data, gboolean exec)
{

	WatchData  *w_data;
	GHashTable *hash;
	gpointer    old_data = NULL;
	
	if (!object)
	{
		return NULL;
	}
	
	hash = (GHashTable *) gtk_object_get_data (object, "WatchData");

	if (!hash)
	{
		data_register_widget (object);
		hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
	}

	w_data = (WatchData *) g_hash_table_lookup (hash, key);

	if (!w_data)
	{
		w_data = g_malloc (sizeof (WatchData));
		w_data->cblist = g_slist_alloc ();
		g_hash_table_insert (hash, key, (gpointer) w_data);
		w_data->lock = FALSE;
		w_data->data = NULL;
		w_data->type = DATA_LITERAL;
		w_data->function_body = NULL;
	}

	old_data = data_get  (GTK_OBJECT (object), key);
	w_data->data = data;
	
	if (exec)
	{
		if (!w_data->lock)
		{
			w_data->lock = TRUE;
			data_exec_watchpoint (hash, object, key, old_data);
			data_exec_watchpoint (watch_keys, object, key, old_data);
			w_data->lock = FALSE;
		}
	}
	return old_data;
}


/*
 * NAME:    data_exec_watchpoint
 *
 * ARGS:    GHashTable *table    -- the list of existing watchpoints
 *          GtkObject  *object   -- the object associated with the watchpoint
 *          gchar      *key      -- the key being changed
 *          gpointer    old_data -- the old value that is being replaced
 *
 * RETURNS: N/A
 *
 * PURPOSE: Executes all watchpoint callbacks attached to the object/key
 *          pair when the object/key data is changed.
 *
 * NOTES:   Calls the function with the object being changed, the data
 *          key being changed, and the old value of the data.
*/

static void
data_exec_watchpoint (GHashTable *table,
		      GtkObject  *object,
		      gchar      *key,
		      gpointer    old_data)
{
	WatchData *wd = NULL;
	GSList    *st;
	WatchFunc *wf;
	int        i, length;
	void     (*func) (GtkObject *object, gchar *key, gpointer data);
	
	if (table)
		wd = (WatchData *) g_hash_table_lookup (table, key);
	
	if (wd && wd->cblist)
	{
		length = g_slist_length (wd->cblist);
		
		for (i=0; i<length; i++)
		{
			st = g_slist_nth (wd->cblist, i);
			wf = (WatchFunc *) st->data;
			if (wf)
			{
				func = (void *) wf->callback;
				if (func)
				{
					func (object, key, wf->cb_data);
				}
			}
		}
	}
}


/*
 * NAME:    data_get
 *
 * ARGS:    GtkObject * -- the object from which to fetch
 *          gchar *     -- the key to fetch to the data within the object
 *
 * RETURNS: gpointer    -- the data stored in the key or NULL on failure
 *
 * PURPOSE: Returns the data from object associated with key.
 *
 * NOTES:   There is no such thing as memory management here.
*/

gpointer
data_get (GtkObject *object, gchar *key)
{
	GHashTable *hash;
	WatchData  *w_data;
	gpointer    result = NULL;
	
	hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
	if (hash)
	{
		w_data = g_hash_table_lookup (hash, key);
		if (w_data)
		{
			result = w_data->data;
		}
	}
	return result;
}


/*
 * NAME:    data_names_list
 *
 * ARGS:    GtkObject * -- the object from which to fetch
 *
 * RETURNS: GList *     -- The list of data names
 *
 * PURPOSE: Returns the list of data names associated with Object.
 *
 * NOTES:
*/

GList *
data_names_list (GtkObject *object)
{
	GHashTable *hash;
	GList      *list;
	
	hash = (GHashTable *) gtk_object_get_data (object, "WatchData");
	
	list = g_list_alloc ();
	
	if (hash)
	{
		g_hash_table_foreach (hash,
				      (GHFunc) data_get_data_name,
				      (gpointer) list);
	}
	list = g_list_remove (list, NULL);
	
	return list;
}


/*
 * NAME:    data_get_data_name
 *
 * ARGS:    gpointer key  -- The key name.
 *          gpointer data -- Ignored.
 *          GList   *list -- The list to wich to append "key"
 *
 * RETURNS:
 *
 * PURPOSE: Appends "key" to "list" (a GList)
 *
 * NOTES:   This is a foreach function for data_names_list
*/

static void
data_get_data_name (gpointer key, gpointer data, GList *list)
{
	g_return_if_fail (list != NULL);
	g_list_append (list, key);
}


/*
 * NAME:    data_get_object_by_name
 *
 * ARGS:    gchar *name -- the name under which the object is stored
 *
 * RETURNS: GtkObject * -- the object stored under "name"
 *
 * PURPOSE: Returns the object stored under "name" in the objects hash
 *
 * NOTES:
*/

GtkObject *
data_get_object_by_name (gchar *name)
{
	gpointer  result;
	
	if (all_objects)
	{
		result = g_hash_table_lookup (all_objects, name);
	}
	else
	{
		result = NULL;
	}
	
	return (GtkObject *) result;
}


/*
 * NAME:    data_name_change
 *
 * ARGS:
 *
 * RETURNS:
 *
 * PURPOSE: Change the name under which "object" is stored
 *
 * NOTES:
*/

static void
data_name_change (GtkObject *object, gchar *key, gpointer blah)
{
	gchar *name;
	gchar *old_name;
	
	if (!all_objects)
	{
		all_objects = g_hash_table_new (g_str_hash, g_str_equal);
	}
	
	if (object)
	{
		name = (gchar *) data_get (object, "name");
		old_name = (gchar *) gtk_object_get_data (object, "oldname");
		
		if (name && strlen(name) > 0)
		{
			if (old_name)
			{
				if (name && data_get_object_by_name (name))
				{
					data_set_no_watchpoints (object, key, old_name);
					g_free (name);
					name = old_name;
					g_hash_table_remove (all_objects, (gpointer) old_name);
				}
				else
				{
					g_free (old_name);
				}
			}
			old_name = g_malloc (strlen (name) + 1);
			strcpy (old_name, name);
			g_hash_table_insert  (all_objects, old_name, object);
			gtk_object_set_data (object, "oldname", old_name);
		}
		else if (old_name)
		{
			g_hash_table_remove (all_objects, (gpointer) old_name);
		}
		
	}
}
