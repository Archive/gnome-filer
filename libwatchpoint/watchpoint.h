/*
 * libwatchpoint: data watchpoint manager
 *
 * Copyright 2000  by Anthony Taylor <tony@searhc.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __WATCHPOINT_DATA_H__
#define __WATCHPOINT_DATA_H__


/*
 * WatchData is used to store information about watchpoints
 * on a per-object basis.  watch_keys stores information about
 * the keys to watch; signals keeps track of the signals the
 * object understands.
*/

typedef struct _WatchData WatchData;
typedef enum _DataType DataType;
typedef struct _WatchFunc WatchFunc;

struct _WatchFunc {
  gpointer callback;
  gpointer cb_data;
};

enum _DataType {
  DATA_UNKNOWN,
  DATA_LITERAL,
  DATA_REF,
  DATA_OBJECT,
  DATA_CHAR,
  DATA_STRING,
  DATA_INT,
  DATA_FLOAT
};

struct _WatchData {
  gpointer  data;
  DataType  type;
  gboolean  lock;
  GSList   *cblist;
  gchar    *function_body;
};

void data_register_widget (GtkObject *object);

void data_set_watchpoint  (GtkObject *object,
			   gchar *key,
			   gpointer cb, 
			   gpointer user_data);

void data_remove_watchpoint (GtkObject *object,
			     gchar *key, 
			     gpointer cb);

gpointer data_initialize  (GtkObject *object, gchar *key, 
			   gpointer data, DataType type);
void     data_set_type    (GtkObject *object, gchar *key, DataType type);
DataType data_get_type    (GtkObject *object, gchar *key);
gpointer data_set         (GtkObject *object, gchar *key, gpointer data);
gpointer data_set_no_watchpoints (GtkObject *object, gchar *key, gpointer data);
gpointer data_get         (GtkObject *object, gchar *key);
GList   *data_names_list  (GtkObject *object);
GtkObject *data_get_object_by_name (gchar *name);

#endif  /* __WATCHPOINT_DATA_H__ */


