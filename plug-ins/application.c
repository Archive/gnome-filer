/*
 * Gnome Filer Application Object
 *
 * Author: 
 *    Anthony Taylor
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Application"

       void        plugin_class_init    (PluginControl *pc);
static void        plugin_class_cleanup (void);
static gchar     * class_name           (void);
static gchar     * unique_name          (void);
static GtkWidget * new_instance         (void);
static GtkWidget * pixmap               (void);
static GtkWidget * mini_pixmap          (void);
static void        menu_pick            (GtkWidget *widget, gpointer data);
static void        hide_children        (gpointer child, gpointer data);


/*
 * Watchpoint Callbacks
*/
static void new_child (GtkObject *window, gchar *key, gpointer data);
static void set_label (GtkObject *window, gchar *key, gpointer data);
static void destroy   (GtkObject *window, gchar *key, gpointer data);
static void set_name  (GtkObject *window, gchar *key, gpointer data);
static void size      (GtkObject *window, gchar *key, gpointer data);
static void status    (GtkObject *window, gchar *key, gpointer data);
static void progress  (GtkObject *window, gchar *key, gpointer data);
static void menu      (GtkObject *window, gchar *key, gpointer data);

/*
 * Miscellaneous support functions
*/

static void menu_cb (GtkWidget *button, gpointer data);
static void resize    (GtkWidget *widget, GtkAllocation *allocation,
		       gpointer user_data);


/*
 * Miscellaneous information
*/

static gint unique_id = 1;

void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (gpointer) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}



static void
plugin_class_cleanup (void)
{

}


/*
 * new_instance --
 *
 *   args: NONE
 *
 *   returns: A new Gnome App toplevel widget
 *
 *   Native Watchpoints:
 *
 *      new_child
 *      destroy
 *      label
 *      message
 *      status
 *      progress
 *      menu
 *
*/

static GtkWidget *
new_instance (void)
{
	GtkWidget *app;
	GtkWidget *hbox;
	GtkWidget *statusbar;
	gchar     *name;

	app  = gnome_app_new ("Gnome Filer Toplevel", NULL);
	gtk_window_set_default_size (GTK_WINDOW (app), 300, 250);

	hbox = gtk_hbox_new (FALSE, 5);
	gnome_app_set_contents (GNOME_APP (app), hbox);

	data_register_widget   (GTK_WIDGET (app));

	statusbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar (GNOME_APP (app), statusbar);

	gtk_object_set_data (hbox, "app", (gpointer) app);
	gtk_object_set_data (app, "container", (gpointer) hbox);

	data_set_watchpoint (GTK_OBJECT (app), "new_child", new_child, hbox);
	data_set_watchpoint (GTK_OBJECT (app), "label", set_label, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "message", set_label, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "name", set_name, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "status", status, statusbar);
	data_set_watchpoint (GTK_OBJECT (app), "progress", progress, statusbar);
	data_set_watchpoint (GTK_OBJECT (app), "destroy", destroy, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "menu", menu, NULL);

	data_initialize (GTK_OBJECT (app), "new_child", NULL, DATA_OBJECT);
	data_initialize (GTK_OBJECT (app), "label", NULL, DATA_CHAR);
	data_initialize (GTK_OBJECT (app), "message", NULL, DATA_CHAR);
	data_initialize (GTK_OBJECT (app), "status", NULL, DATA_CHAR);
	data_initialize (GTK_OBJECT (app), "progress", NULL, DATA_FLOAT);


	name = unique_name ();
	data_set (GTK_OBJECT (app), "label", name);
	g_free (name);

	return app;
}


static gchar *
class_name (void)
{

}


static gchar * 
unique_name (void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	
	if (unique_name)
	{
		g_snprintf (unique_name, GNOME_FILER_NAME_SIZE,
			    "Window %d", unique_id++);
	}
	return unique_name;
}


static GtkWidget * 
pixmap (void)
{

}


static GtkWidget * 
mini_pixmap (void)
{

}


static void
menu_pick (GtkWidget *widget,
	   gpointer data)
{

}


static void
hide_children (gpointer child,
	       gpointer data)
{

}


/*
 * Watchpoint Callbacks
*/
static void 
new_child (GtkObject *app, 
	   gchar *key,
	   gpointer data)
{
	GtkWidget *hbox = GTK_HBOX (data);
	GtkWidget *child;

	child = data_get (app, key);

	if (child && hbox)
	{
		gtk_box_pack_start_defaults (GTK_BOX (hbox), child);
	}
	
}


static void 
set_label (GtkObject *app,
	   gchar     *key,
	   gpointer   data)
{
	gchar     *label;

	label  = data_get (app, "label");

	if (label)
	{
		gtk_window_set_title (GTK_WINDOW(app), label);
	}
	
}


static void 
status (GtkObject *app, 
	gchar     *key,
	gpointer   data)
{
	GtkWidget *statusbar = GTK_WIDGET (data);
	gchar     *status;

	status = (gchar *) data_get (app, key);

	if (status)
		gnome_appbar_set_status (GNOME_APPBAR (statusbar), status);
}


static void 
progress (GtkObject *window,
	  gchar     *key,
	  gpointer   data)
{

}


static void 
destroy (GtkObject *app,
	 gchar     *key,
	 gpointer   data)
{
	gtk_main_quit();
}


static void 
set_name (GtkObject *app,
	  gchar     *key,
	  gpointer   data)
{
	gchar *name;
	
	name = data_get (app, key);
	GNOME_APP (app)->name = g_strdup (name);
	GNOME_APP (app)->prefix = g_strconcat ("/", name, "/", NULL);
}


static void 
size (GtkObject *window, 
      gchar *key, 
      gpointer data)
{

}


static void
menu (GtkObject *app, 
      gchar     *key,
      gpointer   data)
{
	gchar *menu_type;

	menu_type = (gchar *) data_get (app, key);

	if (! g_strncasecmp (menu_type, "min", 3))
	{
		GnomeUIInfo file_menu[] = {
			GNOMEUIINFO_MENU_EXIT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo help_menu[] = {
			GNOMEUIINFO_MENU_ABOUT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo menubar [] = {
			GNOMEUIINFO_MENU_FILE_TREE(file_menu),
			GNOMEUIINFO_MENU_HELP_TREE(help_menu),
			GNOMEUIINFO_END
		};
	    
		gnome_app_create_menus (GNOME_APP (app), menubar);
	}

	if (! g_strncasecmp (menu_type, "def", 3))
	{
		GnomeUIInfo file_menu[] = {
			GNOMEUIINFO_MENU_NEW_ITEM (
				"New doc", 
				"Create a New Gnome-Filer Document", 
				menu_cb,
				(gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_OPEN_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_SAVE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_SAVE_AS_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_CLOSE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_EXIT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo edit_menu[] = {
			GNOMEUIINFO_MENU_CUT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_COPY_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_PASTE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo help_menu[] = {
			GNOMEUIINFO_MENU_ABOUT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo menubar [] = {
			GNOMEUIINFO_MENU_FILE_TREE(file_menu),
			GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
			GNOMEUIINFO_MENU_HELP_TREE(help_menu),
			GNOMEUIINFO_END
		};
	    
		gnome_app_create_menus (GNOME_APP (app), menubar);
	}

	if (! g_strncasecmp (menu_type, "max", 3))
	{
		GnomeUIInfo file_menu[] = {
			GNOMEUIINFO_MENU_NEW_ITEM (
				"New doc", 
				"Create a New Gnome-Filer Document", 
				menu_cb,
				(gpointer) app),
			GNOMEUIINFO_MENU_OPEN_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_SAVE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_SAVE_AS_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_REVERT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_PRINT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_PRINT_SETUP_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_CLOSE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_EXIT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo edit_menu[] = {
			GNOMEUIINFO_MENU_UNDO_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_REDO_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_CUT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_COPY_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_PASTE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_CLEAR_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_SELECT_ALL_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_FIND_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_FIND_AGAIN_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_MENU_REPLACE_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_SEPARATOR,
			GNOMEUIINFO_MENU_PROPERTIES_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};
			
		GnomeUIInfo settings_menu [] = {
			GNOMEUIINFO_MENU_PREFERENCES_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo help_menu[] = {
			GNOMEUIINFO_MENU_ABOUT_ITEM (menu_cb, (gpointer) app),
			GNOMEUIINFO_END
		};

		GnomeUIInfo menubar [] = {
			GNOMEUIINFO_MENU_FILE_TREE(file_menu),
			GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
			GNOMEUIINFO_MENU_SETTINGS_TREE(settings_menu),
			GNOMEUIINFO_MENU_HELP_TREE(help_menu),
			GNOMEUIINFO_END
		};
	    
		gnome_app_create_menus (GNOME_APP (app), menubar);
	}
}


static void
menu_cb (GtkWidget *button, gpointer data)
{
	GtkObject *panel = GTK_OBJECT (data);
	gchar     *path;

	path = gtk_item_factory_path_from_widget (button);

	if (path)
	{
		gchar **strings;

		strings = g_strsplit (path, ">", 1);
		data_set (panel, "menu_select", (gpointer) *path);
		data_set (panel, strings[1], 0);
		g_strfreev(strings);
	}
}


static void
resize (GtkWidget *widget, 
	GtkAllocation *allocation,
	gpointer user_data)
{

}
