/* Gnome Filer
 *
 * Button plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Button"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void set_label  (GtkObject *button, gchar *key, gpointer data);
static void clicked    (GtkObject *button, gchar *key, gpointer data);
static void destroy    (GtkObject *button, gchar *key, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Button %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkWidget *button = NULL;
	GtkWidget *label  = NULL;
	GList     *children;
	gchar     *name;
	gchar     *label_text;
	
	button     = gtk_button_new ();
	children   = g_list_alloc();
	name       = unique_name();
	label_text = g_malloc (sizeof (gchar) * GNOME_FILER_LABEL_SIZE);
	strcpy (label_text, name);
	label      = gtk_label_new (label_text);
	gtk_container_add (GTK_CONTAINER (button), label);
	
	data_set_watchpoint ((GtkObject *) button, "label",
			     set_label, NULL);
	data_set_watchpoint ((GtkObject *) button, "destroy",
			     destroy, NULL);
	data_set_watchpoint ((GtkObject *) button, "clicked",
			     clicked, NULL);
	
	gtk_object_set_data((GtkObject *) button, "children", children);
	gtk_object_set_data((GtkObject *) button, "label", label);
	
	data_initialize ((GtkObject *) button, "class", class_name(), DATA_STRING);
	data_initialize ((GtkObject *) button, "category",
			 (gpointer) OBJECT_CONTROL, DATA_LITERAL);
	data_initialize ((GtkObject *) button, "name", name, DATA_STRING);
	data_initialize ((GtkObject *) button, "label", label_text, DATA_STRING);
	data_initialize ((GtkObject *) button, "x", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) button, "y", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) button, "width", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) button, "height", (gint) 0, DATA_INT);
	
	gtk_widget_show (button);
	gtk_widget_show (label);
	
	return button;
}

static void
clicked (GtkObject *button, gchar *key, gpointer data)
{
}


static GtkWidget *
pixmap (void)
{
	GtkWidget *pixmap;
	
	return NULL;
}


static GtkWidget *
mini_pixmap (void)
{
	
	return NULL;
}


static void
set_label (GtkObject *button, gchar *key, gpointer data)
{
	GtkLabel *label;
	gchar     *name;
	
	name  = (gchar *) data_get (button, key);
	label = (GtkLabel *) gtk_object_get_data ((GtkObject *) button, "label");
	if (label)
	{
		gtk_label_set (label, name);
	}
}


static void
destroy (GtkObject *widget,
	 gchar     *key,
	 gpointer   data)
{
	GtkWidget *label;
	gchar     *name;
	gchar     *class;
	gchar     *label_text;
	GList     *children;
	
	name       = (gchar *) data_get  (widget, "name");
	class      = (gchar *) data_get  (widget, "class");
	label_text = (gchar *) data_get  (widget, "label");
	label      = gtk_object_get_data (widget, "label");
	children   = gtk_object_get_data (widget, "children");
	
	if (name)
	{
		g_free (name);
	}
	
	if (class)
	{
		g_free (class);
	}
	
	if (label_text)
	{
		g_free (label_text);
	}
	
	if (label)
	{
		gtk_widget_destroy (label);
	}
	
	if (children)
	{
		g_list_free (children);
	}
}
