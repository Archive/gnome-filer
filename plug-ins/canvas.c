/* Gnome Filer
 *
 * Canvas plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Canvas"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void clicked    (GtkObject *canvas, gchar *key, gpointer data);
static void add_child  (GtkObject *canvas, gchar *key, gpointer data);
static void child      (GtkObject *canvas, gchar *key, gpointer data);
static void destroy    (GtkObject *canvas, gchar *key, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Canvas %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkWidget *canvas = NULL;
	GList     *children;
	gchar     *name;
	gchar     *label_text;
	
	canvas     = gnome_canvas_new ();
	children   = g_list_alloc();
	name       = unique_name();

	data_set_watchpoint ((GtkObject *) canvas, "destroy",
			     destroy, NULL);
	data_set_watchpoint ((GtkObject *) canvas, "clicked",
			     clicked, NULL);
	data_set_watchpoint ((GtkObject *) canvas, "child",
			     child, NULL);
	
	gtk_object_set_data((GtkObject *) canvas, "children", children);
	
	data_initialize ((GtkObject *) canvas, "class", 
			 class_name(), DATA_STRING);
	data_initialize ((GtkObject *) canvas, "category",
			 (gpointer) OBJECT_CONTROL, DATA_LITERAL);
	data_initialize ((GtkObject *) canvas, "name", name, DATA_STRING);

	data_initialize ((GtkObject *) canvas, "width", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) canvas, "height", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) canvas, "x", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) canvas, "y", (gint) 0, DATA_INT);

	gtk_widget_show (canvas);
	
	return canvas;
}

static void
clicked (GtkObject *canvas,
	 gchar *key,
	 gpointer data)
{
}


static void 
child (GtkObject *canvas, 
       gchar     *key, 
       gpointer   data)
{
	GtkObject *child;

	child = data_get (canvas, key);

	if (child)
		data_set (child, "parent", canvas);
}


static GtkWidget *
pixmap (void)
{
	GtkWidget *pixmap;
	
	return NULL;
}


static GtkWidget *
mini_pixmap (void)
{
	
	return NULL;
}

static void
destroy (GtkObject *widget,
	 gchar     *key,
	 gpointer   data)
{

}
