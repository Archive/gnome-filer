/*
 * Gnome Filer Card Object
 *
 * Author: 
 *    Anthony Taylor
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Cardpanel"

       void        plugin_class_init    (PluginControl *pc);
static void        plugin_class_cleanup (void);
static gchar     * class_name           (void);
static gchar     * unique_name          (void);
static GtkWidget * new_instance         (void);
static GtkWidget * pixmap               (void);
static GtkWidget * mini_pixmap          (void);
static void        menu_pick            (GtkWidget *widget, gpointer data);
static void        place_cards          (GtkWidget *panel);

/*
 * Watchpoint Callbacks
*/
static void set_index (GtkObject *window, gchar *key, gpointer data);
static void set_name  (GtkObject *window, gchar *key, gpointer data);
static void clicked   (GtkObject *window, gchar *key, gpointer data);
static void select_card (GtkObject *window, gchar *key, gpointer data);
static void label     (GtkObject *window, gchar *key, gpointer data);
static void child     (GtkObject *window, gchar *key, gpointer data);
static void destroy   (GtkObject *window, gchar *key, gpointer data);
static void resized   (GtkObject *window, gchar *key, gpointer data);
static void move      (GtkObject *window, gchar *key, gpointer data);

static gint unique_id = 1;

void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (gpointer) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}



static void
plugin_class_cleanup (void)
{

}


/*
 * new_instance --
 *
 *   args: NONE
 *
 *   returns: A new Cardpanel widget
 *
 *   Native Watchpoints:
 *
 *      destroy
 *      name
 *      content      -- The content of the current card
 *      label        -- The label of the current card
 *      index        -- The card on which to work
 *      child        -- The content of the indexed card
 *      last_card    -- The index of the last card.
 *      clicked      -- Activated when a new card is selected
 *                      by the user.
 *
*/

static GtkWidget *
new_instance (void)
{
	GnomeCanvasGroup *down_group;
	GnomeCanvasGroup *up_group;
	GtkWidget *panel;
	gpointer  *cards;
	gchar     *name;

/*
 * "cards" is destined to be a GList containing the
 * list of cards.  For now we just want a permanent
 * place in memory for the GList*.
*/
	cards = g_malloc0 (sizeof (gpointer));

	gtk_widget_push_visual (gdk_imlib_get_visual ());
	gtk_widget_push_colormap (gdk_imlib_get_colormap ());

	panel = gnome_canvas_new ();

	gtk_widget_pop_colormap();
	gtk_widget_pop_visual();
	gtk_widget_show (GTK_WIDGET(panel));


/*
 * Okay, this is crocky.  I shouldn't have to set the scroll
 * region so large just to make sure the widgets stay anchored
 * in their window.  I don't know why the canvas acts so squirrelly
 * with a non-square window when the canvas is set to the same
 * size of the non-square window.
 *
 * What am I doing wrong?
*/
	gnome_canvas_set_scroll_region  (GNOME_CANVAS (panel),
                                              (gdouble) 0.0,
                                              (gdouble) 0.0,
                                              (gdouble) 2000,
                                              (gdouble) 500);

	data_register_widget (GTK_WIDGET (panel));
	data_set_watchpoint (GTK_OBJECT (panel), "index", set_index, cards);
	data_set_watchpoint (GTK_OBJECT (panel), "select", select_card, cards);
	data_set_watchpoint (GTK_OBJECT (panel), "label", label, cards);
	data_set_watchpoint (GTK_OBJECT (panel), "child", child, cards);
	data_set_watchpoint (GTK_OBJECT (panel), "destroy", destroy, NULL);
	data_set_watchpoint (GTK_OBJECT (panel), "name", set_name, NULL);
	data_set_watchpoint (GTK_OBJECT (panel), "HEIGHT", resized, NULL);

	data_initialize (GTK_OBJECT (panel), "name", NULL, DATA_CHAR);
	data_initialize (GTK_OBJECT (panel), "index", NULL, DATA_INT);
	data_initialize (GTK_OBJECT (panel), "last_card", NULL, DATA_INT);
	data_initialize (GTK_OBJECT (panel), "clicked", NULL, DATA_INT);

	name = unique_name ();
	data_set (GTK_OBJECT (panel), "name", (gpointer) name);
	data_set (GTK_OBJECT (panel), "cardlist", (gpointer) cards);

	down_group = GNOME_CANVAS_GROUP (
		gnome_canvas_item_new (
			GNOME_CANVAS_GROUP (GNOME_CANVAS (panel)->root),
			gnome_canvas_group_get_type (),
			"x", 0.0,
			"y", 0.0,
			NULL));

	up_group = GNOME_CANVAS_GROUP (
		gnome_canvas_item_new (
			GNOME_CANVAS_GROUP (GNOME_CANVAS (panel)->root),
			gnome_canvas_group_get_type (),
			"x", 0.0,
			"y", 0.0,
			NULL));

	data_set (GTK_OBJECT (panel), "down_group", (gpointer) down_group);
	data_set (GTK_OBJECT (panel), "up_group", (gpointer) up_group);

	return panel;
}


static gchar *
class_name (void)
{

}


static gchar * 
unique_name (void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	
	if (unique_name)
	{
		g_snprintf (unique_name, GNOME_FILER_NAME_SIZE,
			    "Window %d", unique_id++);
	}
	return unique_name;
}


static GtkWidget * 
pixmap (void)
{

}


static GtkWidget * 
mini_pixmap (void)
{

}


static void
menu_pick (GtkWidget *widget,
	   gpointer data)
{

}



/*
 * Watchpoint Callbacks
*/


/*
 * set_index
 *
 * Set the card on which to operate.  If the requested
 * card does not exist, create a new card and add it to
 * the end of the list of cards.
*/

static void 
set_index (GtkObject *panel, 
	   gchar *key,
	   gpointer data)
{
	GList *cards = (GList *) data;
	gint index;
	gint length;

	index = data_get (panel, key);
	length = g_list_length (cards);

	if (index >= length)
	{
		GtkWidget *vbox;
		GtkWidget *button;
		GnomeCanvasGroup *down_group;
		GnomeCanvasItem  *button_item;

		index = length;
		vbox = gtk_vbox_new (FALSE, 0);
		button = object_new_from_class ("Button");
		data_set (GTK_OBJECT (button), "panel", (gpointer) panel);
		data_set (GTK_OBJECT (button), "vbox", (gpointer) vbox);
		gtk_box_pack_start_defaults (GTK_BOX (vbox), button);

		cards = g_list_append (cards, (gpointer) button);

		data_set_watchpoint (GTK_OBJECT (button), 
				     "clicked", clicked, index);
		data_set_watchpoint (GTK_OBJECT (button), 
				     "motion", move, index);
		
		data_set_no_watchpoints (GTK_OBJECT (panel), 
					 key, (gpointer) index);

		down_group = GNOME_CANVAS_GROUP (
			data_get (GTK_OBJECT (panel), "down_group"));

		button_item = gnome_canvas_item_new (
			down_group,
			gnome_canvas_widget_get_type (),
			"x", 0.0,
			"y", 0.0,
			"widget", vbox,
			NULL);

		data_set (GTK_OBJECT (button), "canvas_item", 
			  (gpointer) button_item);

		gtk_widget_show (button);
		place_cards (panel);
	}
}


static void 
set_name (GtkObject *panel,
	  gchar *key,
	  gpointer data)
{

}


static void 
label (GtkObject *panel,
       gchar     *key,
       gpointer   data)
{
	GList     *cards = (GList *) data;
	GtkWidget *button;
	gint       index;
	gchar     *label;

	index = data_get (panel, "index");
	label = data_get (panel, "label");

	button = (GtkWidget *) g_list_nth_data (cards, index);

	if (label && button)
		data_set (GTK_OBJECT (button), "label", label);
}


static void 
clicked (GtkObject *button,
	 gchar *key,
	 gpointer data)
{
	GtkWidget *panel;

	panel = GTK_WIDGET (data_get (GTK_OBJECT (button), "panel"));
	data_set (GTK_OBJECT (panel), "select", data);
}


static void
select_card (GtkObject *cardpanel,
	gchar     *key,
	gpointer   data)
{
	GList     *cards = (GList *) data;
	GtkObject *button;
	GtkObject *child;
	gchar     *label;
	gint       card;

	card = (gint) data_get (cardpanel, key);
	button = (GtkObject *) g_list_nth_data (cards, card);
	child  = (GtkObject *) data_get (button, "content");
	label  = (gchar *) data_get (button, "label");

	data_set_no_watchpoints (cardpanel, "child", child);
	data_set_no_watchpoints (cardpanel, "label", label);

	data_set (cardpanel, "open_card", (gpointer) card);
	place_cards (cardpanel);
}


static void 
destroy (GtkObject *window, 
	 gchar *key,
	 gpointer data)
{

}


static void 
child (GtkObject *panel, 
       gchar     *key, 
       gpointer   data)
{
	GList     *cards = (GList *) data;
	GtkWidget *button;
	GtkWidget *child;
	gint       index;

	child = data_get (panel, key);      /* key will always be "child" */
	index = data_get (panel, "index");

	button = (GtkWidget *) g_list_nth_data (cards, index);

	if (child && button)
	{
		GtkWidget *sw;
		GtkWidget *vbox;

		vbox = (GtkWidget *) data_get (GTK_OBJECT (button), "vbox");
		sw = gtk_scrolled_window_new(NULL, NULL);

		gtk_scrolled_window_set_policy  (
			GTK_SCROLLED_WINDOW (sw),
			GTK_POLICY_AUTOMATIC,
			GTK_POLICY_AUTOMATIC);
		gtk_scrolled_window_add_with_viewport (
			GTK_SCROLLED_WINDOW (sw), child);
		gtk_box_pack_end_defaults (GTK_BOX (vbox), sw);
		data_set (GTK_OBJECT (button), "content", child);
		data_set (GTK_OBJECT (button), "sw", sw);
	}
}


/*
 * place_cards
 *
 * This moves the cards up or down, depending on the 
 * open card.
 *
 * It uses three canvas groups-- the "up_group" is for
 * the cards that have slid open.  The "down_group" is
 * for the cards at the bottom.  The "sliding_group" is
 * for animating the cards as they slide up or down.
*/

static void
place_cards ( GtkWidget *panel )
{
	GnomeCanvasGroup *down_group;
	GnomeCanvasGroup *up_group;
	GnomeCanvasGroup *sliding_group;
	GnomeCanvasItem  *button_item;
	GtkWidget *button;
	GList     *card_list;
	GList     *sub_list;
	gint       open_card;
	gint       clicked;
	gint       i;
	gint       length;
	gint       height = 0;
	gint       top_button_height;
	gdouble    card_pos = 0.0;
	gint       parent_height;
	gint       parent_width;
	gdouble origin_x;
	gdouble origin_y;

	down_group = GNOME_CANVAS_ITEM (
		data_get (GTK_OBJECT (panel), "down_group"));
	up_group = GNOME_CANVAS_ITEM (
		data_get (GTK_OBJECT (panel), "up_group"));
	
	card_list = (GList *) data_get (GTK_OBJECT (panel), "cardlist");
	open_card = (gint) data_get (GTK_OBJECT (panel), "open_card");
	clicked   = (gint) data_get (GTK_OBJECT (panel), "clicked");
	length    = g_list_length (card_list);

	parent_width  = data_get (GTK_OBJECT (panel), "width");
	parent_height = data_get (GTK_OBJECT (panel), "height");

	if (open_card != clicked)
	{
		/*
		 * This is where some sort of sliding animation
		 * should go, as the card "slides" into place.
		*/
	}

	for (i=0; i<=open_card; i++)
	{
		GtkWidget *button;
		gint this_width;
		gint this_height;

		button = (GtkWidget *) g_list_nth_data (card_list, i);
		if (!button)
			continue;

		button_item = (GnomeCanvasItem *) data_get (
			GTK_OBJECT (button), "canvas_item");
		gnome_canvas_item_reparent (button_item,
					    GNOME_CANVAS_GROUP (up_group));
		gnome_canvas_item_set (button_item,
				       "x", 0.0,
				       "y", (gdouble) height,
				       "width", (gdouble) parent_width,
				       NULL);

		if (button->window)
		{
			gdk_window_get_size (button->window, 
					     &this_width, 
					     &this_height);
			height += this_height;
		}
	}

	top_button_height = height;
	sub_list = g_list_nth (card_list, i);
	height = 0;

	for (i=0; i<(length - open_card); i++)
	{
		GtkWidget *button;
		gint this_width;
		gint this_height;

		button = (GtkWidget *) g_list_nth_data (sub_list, i);
		if (!button)
			continue;

		button_item = (GnomeCanvasItem *) data_get (
			GTK_OBJECT (button), "canvas_item");
		gnome_canvas_item_reparent (button_item,
					    GNOME_CANVAS_GROUP (down_group));
		gnome_canvas_item_set (button_item,
				       "x", 0.0,
				       "y", (gdouble) height,
				       "width", (gdouble) parent_width,
				       NULL);

		if (button->window)
		{
			gdk_window_get_size (button->window, 
					     &this_width, 
					     &this_height);
			height += this_height;
		}
	}

	button = (GtkWidget *) g_list_nth_data (card_list, open_card);
	if (button)
	{
		GtkWidget *sw;
		sw = (GtkWidget *) data_get (GTK_OBJECT (button), "sw");
		if (sw)
		{
			gtk_widget_set_usize (sw,
                                              parent_width,
                                              parent_height - top_button_height - height);
		}
	}
	gnome_canvas_item_set (GNOME_CANVAS_ITEM (up_group),
			       "x", 0.0,
			       "y", 0.0,
			       NULL);
	gnome_canvas_item_set (GNOME_CANVAS_ITEM (down_group),
			       "x", 0.0,
			       "y", (gdouble) parent_height - height,
			       NULL);

	gnome_canvas_scroll_to (GNOME_CANVAS (panel),
				0.0,
				0.0);

}


static void
resized (GtkObject *panel, gchar *key, gpointer data)
{
	GdkWindow *parent_window;
	gint parent_width;
	gint parent_height;
	gint width = (gint) data;
	gint height;

	parent_window = GTK_WIDGET (panel)->window;

	if (parent_window)
	{
		gdk_window_get_size (parent_window, 
				     &parent_width, 
				     &parent_height);

		width  = (gint) data_get (panel, "width");
		height = (gint) data_get (panel, "height");
		
		if (parent_width != width || parent_height != height)
		{
			data_set (panel, "width", 
				  (gpointer) parent_width);
			data_set (panel, "height", 
				  (gpointer) parent_height);
			place_cards (GTK_WIDGET (panel));
		}
	}
}


static void 
move (GtkObject *window, gchar *key, gpointer data)
{

}
