/* Gnome Filer
 *
 * Fixed container plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Fixed"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void move_child (GtkObject *fixed, gchar *key, gpointer data);
static void add_child  (GtkObject *fixed, gchar *key, gpointer data);
static void clicked    (GtkObject *fixed, gchar *key, gpointer data);
static void destroy    (GtkObject *fixed, gchar *key, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_CONTAINER;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Fixed %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkWidget *fixed = NULL;
	gchar     *name;
	
	fixed = gtk_fixed_new ();
	name  = unique_name();

	data_set_watchpoint ((GtkObject *) fixed, "clicked",
			     clicked, NULL);
	data_set_watchpoint ((GtkObject *) fixed, "child",
			     add_child, NULL);
	data_set_watchpoint ((GtkObject *) fixed, "destroy",
			     destroy, NULL);

	data_initialize ((GtkObject *) fixed, "class", class_name(), DATA_STRING);
	data_initialize ((GtkObject *) fixed, "category",
			 (gpointer) OBJECT_CONTAINER, DATA_LITERAL);
	data_initialize ((GtkObject *) fixed, "name", name, DATA_STRING);

	data_initialize ((GtkObject *) fixed, "x", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) fixed, "y", (gint) 0, DATA_INT);
	
	gtk_widget_show (fixed);

	return fixed;
}

static void
add_child (GtkObject *fixed, gchar *key, gpointer data)
{
	GtkWidget *child;

	child = (GtkWidget *) data_get (fixed, key);

	if (child)
	{
		gint16 x, y;

		x = (gint16) data_get (GTK_OBJECT (child), "x");
		y = (gint16) data_get (GTK_OBJECT (child), "y");

		gtk_fixed_put (GTK_FIXED (fixed),
			       GTK_WIDGET (child),
			       x,
			       y);

		data_set_watchpoint (GTK_OBJECT (child), 
				     "x",
				     move_child,
				     (gpointer) fixed);
		data_set_watchpoint (GTK_OBJECT (child),
				     "y",
				     move_child,
				     (gpointer) fixed);
	}
}


static void
move_child (GtkObject *child, gchar *key, gpointer data)
{
	GtkWidget *fixed = GTK_WIDGET (data);
	gint16 x, y;

	if (child)
	{
		x = (gint16) data_get (GTK_OBJECT (child), "x");
		y = (gint16) data_get (GTK_OBJECT (child), "y");
		gtk_fixed_move (GTK_FIXED (fixed),
				child,
				x,
				y);
	}
}


static void
clicked (GtkObject *fixed, gchar *key, gpointer data)
{
}


void
destroy (GtkObject *widget,
	 gchar     *key,
	 gpointer   data)
{
	GtkWidget *label;
	gchar     *name;
	gchar     *class;
	gchar     *label_text;
	GList     *children;
	
	name       = (gchar *) data_get  (widget, "name");
	class      = (gchar *) data_get  (widget, "class");
	children   = gtk_object_get_data (widget, "children");
	
	if (name)
	{
		g_free (name);
	}
	
	if (class)
	{
		g_free (class);
	}
	
	if (children)
	{
		g_list_free (children);
	}
}
