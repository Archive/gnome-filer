/*
 * Gnome Filer Application Object
 *
 * Author: 
 *    Anthony Taylor
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Hpaned"

       void        plugin_class_init    (PluginControl *pc);
static void        plugin_class_cleanup (void);
static gchar     * class_name           (void);
static gchar     * unique_name          (void);
static GtkWidget * new_instance         (void);
static GtkWidget * pixmap               (void);
static GtkWidget * mini_pixmap          (void);
static void        menu_pick            (GtkWidget *widget, gpointer data);
static void        hide_children        (gpointer child, gpointer data);

static void        gtk_paned_remove (GtkContainer *container, gint pane);

/*
 * Watchpoint Callbacks
*/
static void left_child  (GtkObject *window, gchar *key, gpointer data);
static void right_child (GtkObject *window, gchar *key, gpointer data);
static void width       (GtkObject *window, gchar *key, gpointer data);
static void set_label   (GtkObject *window, gchar *key, gpointer data);
static void destroy     (GtkObject *window, gchar *key, gpointer data);
static void size        (GtkObject *window, gchar *key, gpointer data);
static void resize      (GtkWidget *widget, GtkAllocation *allocation,
			 gpointer user_data);

static gint unique_id = 1;

void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (gpointer) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}



static void
plugin_class_cleanup (void)
{

}


/*
 * new_instance --
 *
 *   args: NONE
 *
 *   returns: A new hpaned container
 *
 *   Native Watchpoints:
 *
 *      left_child
 *      right_child
 *      destroy
 *
*/

static GtkWidget *
new_instance (void)
{
	GtkWidget *hpaned;
	gchar     *name;

	hpaned = gtk_hpaned_new ();

	data_register_widget   (GTK_WIDGET (hpaned));

	data_set_watchpoint (GTK_OBJECT (hpaned), "left_child", left_child, NULL);
	data_set_watchpoint (GTK_OBJECT (hpaned), "right_child", right_child, NULL);
	data_set_watchpoint (GTK_OBJECT (hpaned), "divider", width, NULL);
	data_set_watchpoint (GTK_OBJECT (hpaned), "destroy", destroy, NULL);

	name = unique_name ();
	data_set (GTK_OBJECT (hpaned), "label", name);
	g_free (name);

	return hpaned;
}


static gchar *
class_name (void)
{

}


static gchar * 
unique_name (void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	
	if (unique_name)
	{
		g_snprintf (unique_name, GNOME_FILER_NAME_SIZE,
			    "Window %d", unique_id++);
	}
	return unique_name;
}


static GtkWidget * 
pixmap (void)
{

}


static GtkWidget * 
mini_pixmap (void)
{

}


static void
menu_pick (GtkWidget *widget,
	   gpointer data)
{

}


static void
hide_children (gpointer child,
	       gpointer data)
{

}


/*
 * Watchpoint Callbacks
*/
static void 
left_child (GtkObject *hpaned, 
	    gchar *key,
	    gpointer data)
{
	GtkWidget *child = (GtkWidget *) data_get (hpaned, key);

	if (child)
	{
		gtk_paned_remove (GTK_CONTAINER (hpaned), 1);
		gtk_paned_add1 (GTK_PANED (hpaned), child);
	}
}


static void 
right_child (GtkObject *hpaned, 
	     gchar *key,
	     gpointer data)
{
	GtkWidget *child = (GtkWidget *) data_get (hpaned, key);

	if (child)
	{
		gtk_paned_remove (GTK_CONTAINER (hpaned), 2);
		gtk_paned_add2 (GTK_PANED (hpaned), child);
	}
}


static void 
destroy (GtkObject *window, 
	 gchar *key,
	 gpointer data)
{

}


static void 
width (GtkObject *hpaned, 
       gchar     *key, 
       gpointer   data)
{
	gint pos;

	pos = (gint) data_get (hpaned, key);
	gtk_paned_set_position (GTK_PANED (hpaned), pos);
}


static void
resize (GtkWidget *widget, 
	GtkAllocation *allocation,
	gpointer user_data)
{

}



static void
gtk_paned_remove (GtkContainer *container,
		  gint          pane)
{
  GtkPaned *paned;
  
  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_PANED (container));
  
  paned = GTK_PANED (container);
  
  if (pane == 1 && paned->child1)
    {
      gtk_widget_ref (GTK_WIDGET (paned->child1));
      gtk_widget_unparent (paned->child1);
      paned->child1 = NULL;
    }
  else if (pane == 2 && paned->child2)
    {
      gtk_widget_ref (GTK_WIDGET (paned->child2));
      gtk_widget_unparent (paned->child2);
      paned->child2 = NULL;
    }
}
