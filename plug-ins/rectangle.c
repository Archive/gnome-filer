/* Gnome Filer
 *
 * Rectangle (canvas item) plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/*
 * IMPLEMENTATION NOTES
 * ====================
 *
 * Okay, there are a couple of important notes.  Since a canvas item
 * can only be created on a canvas, a new instance cannot happen
 * until the rectangle is attached to a canvas.  Therefore, we don't
 * create a *real* canvas item.  We create a base GtkObject of type
 * GTK_TYPE_OBJECT, and store the information about the object there.
 * Then we can create the real object when it is parented into a 
 * canvas group.
 *
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Rectangle"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void clicked    (GtkObject *rectangle, gchar *key, gpointer data);
static void add_child  (GtkObject *rectangle, gchar *key, gpointer data);
static void parent     (GtkObject *rectangle, gchar *key, gpointer data);
static void size       (GtkObject *rectangle, gchar *key, gpointer data);
static void position   (GtkObject *rectangle, gchar *key, gpointer data);
static void fill_color (GtkObject *rectangle, gchar *key, gpointer data);
static void outline_color (GtkObject *rectangle, gchar *key, gpointer data);
static void outline_width (GtkObject *rectangle, gchar *key, gpointer data);
static void set_label  (GtkObject *rectangle, gchar *key, gpointer data);
static void destroy    (GtkObject *rectangle, gchar *key, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Rectangle %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkObject *rectangle = NULL;
	GList     *children;
	gchar     *name;
	
	rectangle = gtk_object_new (GTK_TYPE_OBJECT, NULL);

	children  = g_list_alloc();
	name      = unique_name();

	data_set_watchpoint (rectangle, "destroy", destroy, NULL);
	data_set_watchpoint (rectangle, "clicked", clicked, NULL);
	data_set_watchpoint (rectangle, "parent", parent, NULL);
	data_set_watchpoint (rectangle, "fill_color", fill_color, NULL);
	data_set_watchpoint (rectangle, "outline_color", outline_color, NULL);
	data_set_watchpoint (rectangle, "outline_width", outline_width, NULL);
	data_set_watchpoint (rectangle, "x", position, NULL);
	data_set_watchpoint (rectangle, "y", position, NULL);
	data_set_watchpoint (rectangle, "height", size, NULL);
	data_set_watchpoint (rectangle, "width", size, NULL);
	data_set_watchpoint (rectangle, "label", set_label, NULL);

	gtk_object_set_data(rectangle, "children", children);
	
	data_initialize (rectangle, "class", class_name(), DATA_STRING);
	data_initialize (rectangle, "category", 
			 (gpointer) OBJECT_CONTROL, DATA_LITERAL);
	data_initialize (rectangle, "name", name, DATA_STRING);

	data_initialize (rectangle, "width", (gint) 0, DATA_INT);
	data_initialize (rectangle, "height", (gint) 0, DATA_INT);

	return rectangle;
}


static void
clicked (GtkObject *rectangle,
	 gchar     *key,
	 gpointer   data)
{
}


static void
parent (GtkObject *rectangle,
	gchar     *key,
	gpointer   data)
{
	GnomeCanvas      *parent;
	GnomeCanvasGroup *rect_group = NULL;
	GnomeCanvasGroup *group = NULL;
	GtkObject        *canvas_text;
	guint x, y, width, height;
	guint fill_color, outline_color;
	guint outline_width;
	gchar *label;

	parent = (GnomeCanvas *) data_get (rectangle, key);

	if (!parent)
	{
		/*
		 * FIXME: Destroy the canvas item
		 */
		gtk_object_set_data (rectangle, "old_parent", NULL);
		return;
	}

	x = (guint) data_get (rectangle, "x");
	y = (guint) data_get (rectangle, "y");
	width  = (guint) data_get (rectangle, "width");
	height = (guint) data_get (rectangle, "height");
	outline_color = (guint) data_get (rectangle, "outline_color");
	fill_color = (guint) data_get (rectangle, "fill_color");
	outline_width = (guint) data_get (rectangle, "outline_width");
	label = (gchar *) data_get (rectangle, "label");

	if (GNOME_IS_CANVAS(parent))
	{
		group = parent->root;
	}
	else if (GNOME_IS_CANVAS_GROUP (parent))
	{
		group = (GnomeCanvasGroup *) parent;
	}

	if (!group)
		return;

	rect_group = (GnomeCanvasGroup *) gtk_object_get_data (rectangle, "rect_group");

	if (!rect_group)
	{
		GnomeCanvasItem  *item;
		GtkObject        *canvas_text;

		rect_group = gnome_canvas_item_new (group,
						    GNOME_TYPE_CANVAS_GROUP,
						    "x", (double) x,
						    "y", (double) y,
						    NULL);

		item = gnome_canvas_item_new (rect_group,
					      GNOME_TYPE_CANVAS_RECT,
					      "x1", (double) 0,
					      "y1", (double) 0,
					      "x2", (double) width,
					      "y2", (double) height,
					      "outline_color_rgba", outline_color,
					      "fill_color_rgba", fill_color,
					      "width_pixels", outline_width,
					      NULL);


		canvas_text = object_new_from_class ("Text");

		data_set (GTK_OBJECT (canvas_text), "x", (gpointer) 5);
		data_set (GTK_OBJECT (canvas_text), "y", (gpointer) 5);
		data_set (GTK_OBJECT (canvas_text), "fill_color", (gpointer) 0x101000);
		data_set (GTK_OBJECT (canvas_text), "label", label);
		data_set (GTK_OBJECT (canvas_text), "parent", (gpointer) rect_group);

		gtk_object_set_data (rectangle, "rect_group", (gpointer) rect_group);
		gtk_object_set_data (rectangle, "item", (gpointer) item);
		gtk_object_set_data (rectangle, "text", (gpointer) canvas_text);
	}
	else
	{
		gnome_canvas_item_reparent (rect_group, group);
	}
}


static void 
size (GtkObject *rectangle, 
      gchar     *key, 
      gpointer   data)
{
	GnomeCanvasItem  *item;
	guint x, y, width, height;

	item = (GnomeCanvasItem *) gtk_object_get_data (rectangle, "item");
	if (!item)
		return;
 
	width  = (guint) data_get (rectangle, "width");
	height = (guint) data_get (rectangle, "height");

	gnome_canvas_item_set ( item,
				"x2", (double) width,
				"y2", (double) height,
				NULL);
}


static void 
position (GtkObject *rectangle, 
	  gchar     *key, 
	  gpointer   data)
{
	GnomeCanvasItem  *rect_group;
	guint x, y, width, height;

	rect_group = (GnomeCanvasItem *) gtk_object_get_data (rectangle, "rect_group");
	if (!rect_group)
		return;
 
	x = (guint) data_get (rectangle, "x");
	y = (guint) data_get (rectangle, "y");

	gnome_canvas_item_set ( rect_group,
				"x", (double) x,
				"y", (double) y,
				NULL);
}


static void
fill_color (GtkObject *rectangle, 
	    gchar     *key, 
	    gpointer   data)
{
	GnomeCanvasItem *item;
	guint fill_color;

	item = (GnomeCanvasItem *) gtk_object_get_data (rectangle, "item");
	if (!item)
		return;
 
	fill_color = (guint) data_get (rectangle, "fill_color");

	gnome_canvas_item_set ( item,
				"fill_color_rgba", fill_color,
				NULL);
}


static void
outline_color (GtkObject *rectangle,
	       gchar     *key,
	       gpointer   data)
{
	GnomeCanvasItem *item;
	guint outline_color;

	item = (GnomeCanvasItem *) gtk_object_get_data (rectangle, "item");
	if (!item)
		return;
 
	outline_color = (guint) data_get (rectangle, "outline_color");

	gnome_canvas_item_set ( item,
				"outline_color_rgba", outline_color,
				NULL);
}


static void
outline_width (GtkObject *rectangle,
	       gchar     *key,
	       gpointer   data)
{
	GnomeCanvasItem *item;
	guint outline_width;

	item = (GnomeCanvasItem *) gtk_object_get_data (rectangle, "item");
	if (!item)
		return;
 
	outline_width = (guint) data_get (rectangle, "outline_width");

	gnome_canvas_item_set ( item,
				"outline_width", outline_width,
				NULL);
}


static GtkWidget *
pixmap (void)
{
	GtkWidget *pixmap;
	
	return NULL;
}


static GtkWidget *
mini_pixmap (void)
{
	
	return NULL;
}


static void
set_label (GtkObject *rectangle, gchar *key, gpointer data)
{
	GtkObject *text;
	gchar     *label;

	text = GTK_OBJECT (gtk_object_get_data (rectangle, "text"));
	if (text)
	{
		label = data_get (rectangle, key);
		data_set (text, "label", label);
	}
}


static void
destroy (GtkObject *rectangle,
	 gchar     *key,
	 gpointer   data)
{

}
