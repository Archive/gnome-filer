/* Gnome Filer
 *
 * Text (canvas item) plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/*
 * IMPLEMENTATION NOTES
 * ====================
 *
 * Okay, there are a couple of important notes.  Since a canvas item
 * can only be created on a canvas, a new instance cannot happen
 * until the rectangle is attached to a canvas.  Therefore, we don't
 * create a *real* canvas item.  We create a base GtkObject of type
 * GTK_TYPE_OBJECT, and store the information about the object there.
 * Then we can create the real object when it is parented into a 
 * canvas group.
 *
 * SPECIAL NOTE: The "text" here refers to the canvas object, and not
 *               a null-terminated string.
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Text"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void clicked    (GtkObject *text, gchar *key, gpointer data);
static void add_child  (GtkObject *text, gchar *key, gpointer data);
static void parent     (GtkObject *text, gchar *key, gpointer data);
static void size       (GtkObject *text, gchar *key, gpointer data);
static void fill_color (GtkObject *text, gchar *key, gpointer data);
static void outline_color (GtkObject *text, gchar *key, gpointer data);
static void outline_width (GtkObject *text, gchar *key, gpointer data);
static void set_label  (GtkObject *text, gchar *key, gpointer data);
static void destroy    (GtkObject *text, gchar *key, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Text %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkObject *text = NULL;
	GList     *children;
	gchar     *name;
	
	text = gtk_object_new (GTK_TYPE_OBJECT, NULL);

	children  = g_list_alloc();
	name      = unique_name();

	data_set_watchpoint (text, "destroy", destroy, NULL);
	data_set_watchpoint (text, "clicked", clicked, NULL);
	data_set_watchpoint (text, "parent", parent, NULL);
	data_set_watchpoint (text, "fill_color", fill_color, NULL);
	data_set_watchpoint (text, "outline_color", outline_color, NULL);
	data_set_watchpoint (text, "outline_width", outline_width, NULL);
	data_set_watchpoint (text, "x", size, NULL);
	data_set_watchpoint (text, "y", size, NULL);
	data_set_watchpoint (text, "height", size, NULL);
	data_set_watchpoint (text, "width", size, NULL);
	data_set_watchpoint (text, "label", set_label, NULL);

	gtk_object_set_data(text, "children", children);
	
	data_initialize (text, "class", class_name(), DATA_STRING);
	data_initialize (text, "category", 
			 (gpointer) OBJECT_CONTROL, DATA_LITERAL);
	data_initialize (text, "name", name, DATA_STRING);

	data_initialize (text, "width", (gint) 0, DATA_INT);
	data_initialize (text, "height", (gint) 0, DATA_INT);

	return text;
}


static void
clicked (GtkObject *text,
	 gchar     *key,
	 gpointer   data)
{
}


static void
parent (GtkObject *text,
	gchar     *key,
	gpointer   data)
{
	GnomeCanvas      *parent;
	GnomeCanvasItem  *item;
	GnomeCanvasGroup *group = NULL;
	guint  x, y, width, height;
	guint  fill_color, outline_color;
	guint  outline_width;
	gchar *label;

	parent = (GnomeCanvas *) data_get (text, key);

	if (!parent)
	{
		/*
		 * FIXME: Destroy the canvas item
		 */
		gtk_object_set_data (text, "old_parent", NULL);
		return;
	}

	x = (guint) data_get (text, "x");
	y = (guint) data_get (text, "y");
	width  = (guint) data_get (text, "width");
	height = (guint) data_get (text, "height");
	outline_color = (guint) data_get (text, "outline_color");
	fill_color = (guint) data_get (text, "fill_color");
	outline_width = (guint) data_get (text, "outline_width");
	label = (gchar *) data_get (text, "label");

	if (GNOME_IS_CANVAS(parent))
	{
		group = parent->root;
	}
	else if (GNOME_IS_CANVAS_GROUP (parent))
	{
		group = (GnomeCanvasGroup *) parent;
	}

	if (!group)
		return;

	item = (GnomeCanvasItem *) gtk_object_get_data (text, "item");

	if (!item)
	{
		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_TEXT,
					      "font", "fixed",
					      "x", (double) x,
					      "y", (double) y,
					      "fill_color_rgba", fill_color,
					      "text", label,
					      NULL);
		gtk_object_set_data (text, "item", (gpointer) item);
	}
	else
	{
		gnome_canvas_item_reparent (item, group);
	}
}


static void 
size (GtkObject *text, 
      gchar     *key, 
      gpointer   data)
{
	GnomeCanvasItem *item;
	guint x, y, width, height;

	item = (GnomeCanvasItem *) gtk_object_get_data (text, "item");
	if (!item)
		return;
 
	x = (guint) data_get (text, "x");
	y = (guint) data_get (text, "y");
	width  = (guint) data_get (text, "width");
	height = (guint) data_get (text, "height");

	gnome_canvas_item_set ( item,
				"x1", (double) x,
				"y1", (double) y,
				"x2", (double) x + width,
				"y2", (double) y + height,
				NULL);
}


static void
fill_color (GtkObject *text, 
	    gchar     *key, 
	    gpointer   data)
{
	GnomeCanvasItem *item;
	guint fill_color;

	item = (GnomeCanvasItem *) gtk_object_get_data (text, "item");
	if (!item)
		return;
 
	fill_color = (guint) data_get (text, "fill_color");

	gnome_canvas_item_set ( item,
				"fill_color_rgba", fill_color,
				NULL);
}


static void
outline_color (GtkObject *text,
	       gchar     *key,
	       gpointer   data)
{
	GnomeCanvasItem *item;
	guint outline_color;

	item = (GnomeCanvasItem *) gtk_object_get_data (text, "item");
	if (!item)
		return;
 
	outline_color = (guint) data_get (text, "outline_color");

	gnome_canvas_item_set ( item,
				"outline_color_rgba", outline_color,
				NULL);
}


static void
outline_width (GtkObject *text,
	       gchar     *key,
	       gpointer   data)
{
	GnomeCanvasItem *item;
	guint outline_width;

	item = (GnomeCanvasItem *) gtk_object_get_data (text, "item");
	if (!item)
		return;
 
	outline_width = (guint) data_get (text, "outline_width");

	gnome_canvas_item_set ( item,
				"outline_width", outline_width,
				NULL);
}


static GtkWidget *
pixmap (void)
{
	GtkWidget *pixmap;
	
	return NULL;
}


static GtkWidget *
mini_pixmap (void)
{
	
	return NULL;
}


static void
set_label (GtkObject *text, gchar *key, gpointer data)
{
	GnomeCanvasItem  *item;
	gchar *label;

	if (text)
	{
		item = gtk_object_get_data (text, "item");
		if (item)
		{
			label = data_get (text, key);
			gnome_canvas_item_set (item,
					       "text", label,
					       NULL);
		}
	}
}


static void
destroy (GtkObject *text,
	 gchar     *key,
	 gpointer   data)
{

}
