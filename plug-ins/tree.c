/* Gnome Filer
 *
 * Tree plugin definition
 *
 * Copyright 2000 by Anthony Taylor <tonyt@ptialaska.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <gnome.h>
#include "constants.h"
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"

#define CLASS_NAME "Tree"

       void        plugin_class_init (PluginControl *pc);
static void        plugin_class_cleanup (void);
static GtkWidget * new_instance (void);
static GtkWidget * pixmap (void);
static GtkWidget * mini_pixmap (void);
static gchar     * class_name (void);
static gchar     * unique_name (void);

/*
 * Watchpoint Callbacks
*/
static void select_path (GtkObject *tree, gchar *key, gpointer data);
static void clicked     (GtkObject *tree, gchar *key, gpointer data);
static void destroy     (GtkObject *tree, gchar *key, gpointer data);

static void add_path (GtkObject *tree, gchar *path, GHashTable *pathlist);
static void select_item (GtkItem *item, gpointer data);
static void deselect_item (GtkItem *item, gpointer data);

static gint unique_id = 1;


void
plugin_class_init (PluginControl *pc)
{
	ObjectControl *oc;

	oc = (ObjectControl *) g_malloc0 (sizeof (ObjectControl));
	if (!oc)
		g_error ("========> Out of memory!\n");

	pc->extended_info = (void *) oc;

	pc->type      = PLUGIN_OBJECT;
	pc->version   = PLUGIN_VERSION_LEVEL;
	pc->classname = CLASS_NAME;
	pc->plugin_class_cleanup = plugin_class_cleanup;

	oc->object_type  = OBJECT_TOPLEVEL;
	oc->version      = OBJECT_VERSION_LEVEL;
	oc->new_instance = new_instance;
}


static gchar *
class_name (void)
{
	gchar *name;
	
	name = g_malloc (strlen(CLASS_NAME));
	strcpy (name, CLASS_NAME);
	return name;
}


static gchar *
unique_name(void)
{
	gchar *unique_name;
	
	unique_name = g_malloc (sizeof(gchar) * GNOME_FILER_NAME_SIZE);
	if (unique_name)
	{
		sprintf (unique_name, "Tree %d", unique_id++);
	}
	
	return unique_name;
}


static GtkWidget *
new_instance (void)
{
	GtkWidget  *tree = NULL;
	GHashTable *pathlist;
	gchar      *name;
	gchar      *label_text;
	
	tree     = gtk_tree_new ();
	pathlist = g_hash_table_new (g_str_hash, g_str_equal);
	name     = unique_name  ();

	data_set_watchpoint ((GtkObject *) tree, "destroy",
			     destroy, NULL);
	data_set_watchpoint ((GtkObject *) tree, "clicked",
			     clicked, NULL);
	data_set_watchpoint ((GtkObject *) tree, "path",
			     select_path, (gpointer) pathlist);

	data_initialize ((GtkObject *) tree, "class", 
			 class_name(), DATA_STRING);
	data_initialize ((GtkObject *) tree, "category",
			 (gpointer) OBJECT_CONTROL, DATA_LITERAL);
	data_initialize ((GtkObject *) tree, "name", name, DATA_STRING);

	data_initialize ((GtkObject *) tree, "width", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) tree, "height", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) tree, "x", (gint) 0, DATA_INT);
	data_initialize ((GtkObject *) tree, "y", (gint) 0, DATA_INT);

	gtk_widget_show (tree);

	return tree;
}

static void
clicked (GtkObject *tree,
	 gchar *key,
	 gpointer data)
{
}


/*
 * select_path
 *
 * This is the interesting bit.  By setting the watchpoint
 * "path" to a relevent pathname (say, "/Tree/Subtree/Item"),
 * all the appropriate pieces are built to create the "Item"
 * in the correct spot.  That is, "Tree" (the topmost part of
 * the path) will be built; then "Subtree" will be created and
 * added to "Tree," and finally, "Item" is created and added
 * to "Subtree."
 *
 * If the path exists, then it merely sets "Item" as the default
 * target of future callbacks (say, "label," or "icon").
*/

static void select_path (GtkObject *tree, 
			 gchar     *key,
			 gpointer   data)
{
	GHashTable *pathlist = (GHashTable *) data;
	gchar *path;

	path = (gchar *) data_get (tree, key);

	if (!g_hash_table_lookup (pathlist, path))
	{
		add_path (tree, path, data);
	}

}

static void
add_path (GtkObject  *tree,
	  gchar      *setpath,
	  GHashTable *pathlist)
{
	GtkWidget  *treeitem;
	GtkWidget  *parent_item;
	GtkWidget  *hbox;
	GtkTree    *parent_tree;
	GtkWidget  *label;
	gchar      *path;
	gchar      *subtree;
	gchar      *parent_path;
	gchar      *itemlabel;
	gchar     **pathelems;
	int         index = 0;

	path = g_strdup (setpath);
	pathelems = g_strsplit (path, "/", NULL);
	
	while (pathelems[index])
		itemlabel = pathelems[index++];
	treeitem = gtk_tree_item_new ();
	hbox  = gtk_hbox_new (FALSE, NULL);
	label = gtk_label_new (itemlabel);
	gtk_widget_show (hbox);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, NULL);
	gtk_container_add (GTK_CONTAINER(treeitem), hbox);

	--index;
	pathelems[index] = NULL;
	
	parent_path = g_strjoinv ("/", pathelems);
	g_strfreev (pathelems);
	
	if (!strlen(parent_path))
	{
		parent_tree = tree;
	}
	else
	{
		parent_item = (GtkTreeItem *) g_hash_table_lookup (pathlist, (gpointer) parent_path);
		
/*
 * If the parent does not yet exist, create it
 */
		if (!parent_item)
		{
			add_path (tree, parent_path, pathlist);
			parent_item = (GtkTreeItem *) g_hash_table_lookup (pathlist, (gpointer) parent_path);
		}
		
		g_free (parent_path);
		
		parent_tree = (GtkTree *) gtk_object_get_data (GTK_OBJECT (parent_item), "subtree");
	}
	if (!parent_tree)
	{
		parent_tree = gtk_tree_new ();
		gtk_tree_item_set_subtree (parent_item, parent_tree);
		gtk_object_set_data (GTK_OBJECT (parent_item),
				     "subtree",
				     (gpointer) parent_tree);
		gtk_widget_show (parent_tree);

	}
	
	gtk_tree_append (parent_tree, GTK_WIDGET (treeitem));

	data_set (tree, path, (gpointer) treeitem);
	gtk_object_set_data (GTK_OBJECT (treeitem), "parent_tree", parent_tree);

	g_hash_table_insert (pathlist, 
			     (gpointer) path, 
			     (gpointer) treeitem);

	gtk_signal_connect (GTK_OBJECT (treeitem),
			    "select",
			    (GtkSignalFunc) select_item,
			    (gpointer) path);

	gtk_signal_connect (GTK_OBJECT (treeitem),
			    "deselect",
			    (GtkSignalFunc) deselect_item,
			    (gpointer) path);

	gtk_widget_show (treeitem);
}


static void
select_item (GtkItem *item,
	     gpointer data)
{
	gchar   *path = (gchar *) data;
	GtkTree *root_tree;
	GtkTree *parent_tree;

	parent_tree = (GtkTree *) gtk_object_get_data (GTK_OBJECT (item), 
						       "parent_tree");
	root_tree = GTK_TREE_ROOT_TREE (parent_tree);
	data_set (GTK_OBJECT (root_tree), path, (gpointer) TRUE);
}


static void
deselect_item (GtkItem *item,
	      gpointer data)
{
	gchar *path = (gchar *) data;
	GtkTree *root_tree;
	GtkTree *parent_tree;

	parent_tree = (GtkTree *) gtk_object_get_data (GTK_OBJECT (item), 
						       "parent_tree");
	root_tree = GTK_TREE_ROOT_TREE (parent_tree);
	data_set (GTK_OBJECT (root_tree), path, (gpointer) FALSE);
}


static GtkWidget *
pixmap (void)
{
	GtkWidget *pixmap;
	
	return NULL;
}


static GtkWidget *
mini_pixmap (void)
{
	
	return NULL;
}

static void
destroy (GtkObject *widget,
	 gchar     *key,
	 gpointer   data)
{

}
