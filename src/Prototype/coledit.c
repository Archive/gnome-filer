/* Edit columns in a given table
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */

#include <gnome.h>

#include "filerwindow.h"
#include "dbfe.h"
#include "coledit.h"
#include "uimisc.h"


static GtkWidget *createTypeAuxWidget(DataTypeSpec type);
static GtkWidget *createTypeAuxLabel(DataTypeSpec type);
static gchar **createColumnEditWindowRow(DBColumn field);

/* update auxilary line in data selector based on the new type */
static void
setTypeSelectorAuxLine(GtkWidget *frame, DataTypeSpec type)
{
    GtkWidget      *table;
    GtkWidget      *align;
    GtkWidget      *auxwidget, *oauxwidget;
    GtkWidget      *auxlabel, *oauxlabel;
    DataTypeSpec   oldtype, newtype;

    /* get the frame around of the data type selection area   */
    /* it contains object data for all the interesting things */
    table      =  gtk_object_get_data(GTK_OBJECT(frame), "table");
    oauxlabel  =  gtk_object_get_data(GTK_OBJECT(frame), "auxlabel");
    oauxwidget =  gtk_object_get_data(GTK_OBJECT(frame), "auxwidget");

    /* destroy old auxilary line */
    if (oauxwidget)
	gtk_widget_destroy(oauxwidget);
    if (oauxlabel)
	gtk_widget_destroy(oauxlabel);

    auxlabel  =  createTypeAuxLabel(type);
    auxwidget =  createTypeAuxWidget(type);
    align = gtk_alignment_new(0.5, 0.0, 0.0, 0.0);
    gtk_container_add(GTK_CONTAINER(align), auxlabel);
    gtk_table_attach(GTK_TABLE(table), align, 0, 1, 1, 2,
		     GTK_FILL|GTK_EXPAND, 0, 0, 0);
    gtk_table_attach(GTK_TABLE(table), auxwidget, 1, 2, 1, 2,
		     GTK_FILL|GTK_EXPAND, 0, 0, 0);

    oldtype = gtk_object_get_data(GTK_OBJECT(frame), "type");
    if (oldtype)
	g_free(oldtype);
    newtype = g_malloc(sizeof(*newtype));
    memcpy(newtype, type, sizeof(*newtype));
    gtk_object_set_data(GTK_OBJECT(frame), "type", newtype);
    gtk_object_set_data(GTK_OBJECT(frame), "auxlabel", align);
    gtk_object_set_data(GTK_OBJECT(frame), "auxwidget", auxwidget);

    gtk_widget_show_all(table);
}

/* handle when user changes data type in the column edit form     */
/* may involve redrawing the datatype frame to remove/add         */
/* auxilary widgets for configuring the type (like fixed str len) */
static void
setTypeSelectionCB(GtkWidget *widget, gpointer data)
{
    DataTypeEnum   type;
    DataTypeSpec   spec;
    GtkWidget      *frame;

    /* the selected menu entry has object data telling us the DataTypeEnum */

    type   = (DataTypeEnum) gtk_object_get_data(GTK_OBJECT(widget),"datatype");
    spec   = getDataTypeDefaultSpec(type);
    frame  = GTK_WIDGET(data);

    setTypeSelectorAuxLine(frame, spec);
    return;
}




/* sets the data selector widget to the particulr data type               */
/* selector frame widget                                                  */ 
void
setTypeSelectorType(GtkWidget *w, DataTypeSpec type)
{
    GHashTable *hash;
    GtkWidget  *omenu;
    gint       itemno;

    hash = (GHashTable *)gtk_object_get_data(GTK_OBJECT(w), "menuhash");
    omenu = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(w), "omenu");
    itemno = (gint)g_hash_table_lookup(hash, GINT_TO_POINTER(type->type));

    gtk_option_menu_set_history(GTK_OPTION_MENU(omenu), itemno); 
    gtk_widget_show(omenu);

    /* also have to kick auxlilary line too */
    setTypeSelectorAuxLine( w, type );
}


/* allocates a DataTypeSpec and sets it according to the passed data type */
/* selector frame widget                                                  */ 
DataTypeSpec
getTypeSelectorSpec(GtkWidget *w)
{
    DataTypeSpec spec, type;
    GtkWidget    *auxwidget;
    GtkWidget    *item;

    /* type stored in the widget has the datatypeenum, but not the */
    /* data type specific configuration data.                      */
    type = (DataTypeSpec) gtk_object_get_data(GTK_OBJECT(w), "type");
    spec = g_malloc(sizeof(*spec));
    spec->type = type->type;

    /* get data type specific configuration data */
    auxwidget = gtk_object_get_data(GTK_OBJECT(w), "auxwidget");
    switch (spec->type) {
      case DATA_CHARN:
	  item = gtk_object_get_data(GTK_OBJECT(auxwidget), "spinner");
	  spec->spec = GINT_TO_POINTER(
	      gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(item)));
	  break;
      default:
	  spec->spec = NULL;
    }

    return spec;
}


/* return the label required for the auxilary line for datatype frame */
static GtkWidget
*createTypeAuxLabel(DataTypeSpec type)
{
    GtkWidget *hbox;
    GtkWidget *label;

    hbox = gtk_hbox_new(FALSE, 0);

    switch (type->type) {
      case DATA_CHARN:
	label = gtk_label_new("Max Length");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

	break;
      default:
	  break;
    }

    return hbox;
}

/* return the widget required for the auxilary line for datatype frame */
static GtkWidget
*createTypeAuxWidget(DataTypeSpec type)
{
    GtkWidget *hbox;
    GtkWidget *item;
    GtkWidget *align;
    GtkAdjustment *adj;
    gint      def;

    g_return_val_if_fail( type != NULL, NULL );

    hbox = gtk_hbox_new(FALSE, 0);

    switch (type->type) {
      case DATA_CHARN:
	def = (gint)type->spec;
	if (def < 1 || def > 80)
	    def = 20;
	type->spec = GINT_TO_POINTER(def);
	adj = (GtkAdjustment *) gtk_adjustment_new (def, 1.0,
						    80.0, 1.0,
						    10.0, 0.0);
	item  = gtk_spin_button_new(adj, 1.0, 0.0);
	gtk_spin_button_set_shadow_type (GTK_SPIN_BUTTON(item),
					 GTK_SHADOW_NONE);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(item), TRUE);
	align = gtk_alignment_new(1.0, 0.0, 0.0, 0.0);
	gtk_container_add(GTK_CONTAINER(align), item);

	gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);
	gtk_object_set_data(GTK_OBJECT(hbox), "spinner", item);
	break;
      default:
/*	GtkWidget *label;
	label = gtk_label_new("No options");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
*/
	break;
    }

    return hbox;
}

static gint
my_datatype_equal( gconstpointer v, gconstpointer v2 )
{
    return ((gint)v == (gint) v2);
}

/* create a box which has the proper GUI for */
/* selecting a data type.                    */
/* defaults to first available data type     */
/* returned widget has several object data associated with it  */
/*    "table"      - table to pack data selector widget inside */
/*    "omenu"      - option menu holding 'menu' below */
/*    "menuhash"   - hash table linking data type to menu items */
/*    "auxlabel"   - label for auxilary data line */
/*    "auxwidget"  - widget for auxilary data line */
/*    "type"       - DataTypeEnum value of current selected menu entry */

static GtkWidget
*createTypeSelector(void)
{
    GtkWidget *table;
    GtkWidget *menu, *omenu;
    GtkWidget *item;
    GtkWidget *label;
    GtkWidget *frame;

    DataTypeEnum type;
    DataTypeSpec defaultType=NULL;

    gchar     *name;
    gint      itemno=0;
    GHashTable *hash;

    frame = gtk_frame_new("Data Type Specification");
    gtk_container_border_width(GTK_CONTAINER(frame), GNOME_PAD);

    table = gtk_table_new(2, 3, FALSE);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_table_set_row_spacings(GTK_TABLE(table), GNOME_PAD);
    gtk_table_set_col_spacings(GTK_TABLE(table), GNOME_PAD);
    gtk_object_set_data(GTK_OBJECT(frame), "table", table);

    type = getFirstDataType();
    if (type != DATA_INVALID) {
	hash = g_hash_table_new(NULL, my_datatype_equal);
	omenu = gtk_option_menu_new();
	menu = gtk_menu_new();
	while (type != DATA_INVALID) {
	    name = getDataTypeName(type);
	    item = gtk_menu_item_new_with_label (name);
	    gtk_menu_append (GTK_MENU(menu), item);
	    gtk_signal_connect (GTK_OBJECT(item), "activate", 
				GTK_SIGNAL_FUNC(setTypeSelectionCB), frame);
	    gtk_object_set_data(GTK_OBJECT(item), "datatype", 
				GINT_TO_POINTER(type));
	    g_hash_table_insert(hash, GINT_TO_POINTER(type),
				GINT_TO_POINTER(itemno));

	    if (!itemno)
		defaultType = getDataTypeDefaultSpec(type);

	    type = getNextDataType(type);
	    itemno++;
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU(omenu), menu);
	gtk_object_set_data(GTK_OBJECT(frame), "omenu", omenu);
	gtk_object_set_data_full(GTK_OBJECT(frame), "menuhash", hash, 
				 (GtkDestroyNotify)g_hash_table_destroy);

	label = gtk_label_new("Type");
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		     GTK_FILL|GTK_EXPAND, 0, 0, 0);
	gtk_table_attach(GTK_TABLE(table), omenu, 1, 2, 0, 1,
		     GTK_FILL|GTK_EXPAND, 0, 0, 0);

	/* see if we need a auxilary line */
	setTypeSelectorAuxLine(frame, defaultType);
	setTypeSelectorType(frame, defaultType);
	g_free(defaultType);
    }

    return frame;
}



static void
fieldCancelButtonCB (GtkWidget *widget, gpointer data)
{
    gtk_widget_destroy(data);
}

static void
fieldOkButtonCB (GtkWidget *widget, gpointer data)
{
    GtkWidget  *temp;
    GtkWidget  *invoker;

    DataTypeSpec type;
    DBColumn   field;
    DBTable    table;
    FilerWindow mainWin;

    invoker = (GtkWidget *)getWidget (data, "invoker_widget");
    table = (DBTable)gtk_object_get_data (GTK_OBJECT (invoker), "table_def");

    if (!gtk_object_get_data (GTK_OBJECT (data), "edit_field")) {
	field = createColumn();
    } else {
	field = (DBColumn)gtk_object_get_data (GTK_OBJECT (data), "field_def");
    }

    temp = (GtkWidget *)getWidget(data, "field_name");
    setColumnName(field, gtk_entry_get_text (GTK_ENTRY (temp)));
    
    printf("Field name -> %s\n",gtk_entry_get_text (GTK_ENTRY (temp)));
    
    temp = (GtkWidget *)getWidget(data, "field_comment");
    setColumnComment(field, gtk_entry_get_text (GTK_ENTRY (temp)));
    
    printf("Field comment -> %s\n", gtk_entry_get_text (GTK_ENTRY (temp)));
    
#if 0
    temp = (GtkWidget *)getWidget(data, "field_required");
#else
    temp = (GtkWidget *)getWidget(data, "field_required_y");
#endif
    setColumnRequiredFlag(field,  (GTK_TOGGLE_BUTTON(temp)->active) != 0);
    
    printf("Field required -> %c\n", ((GTK_TOGGLE_BUTTON(temp)->active) ? 'Y' : 'N' ));
    
    
    /* field type little more complicated than just a string */
    /* need to get the data type enum and a pointer to its   */
    /* data type specific configuration (like maxlen of str) */
    temp = (GtkWidget *)getWidget(data, "field_type");
    type = getTypeSelectorSpec(temp);
    setColumnDataType(field,  type );
    
    printf("Field type    -> %d\n",type->type);
    printf("Field spec    -> %p\n",type->spec);
    
    g_free(type);

    /* if we just changed an existing table entry, just refresh table*/
    if (!gtk_object_get_data (GTK_OBJECT (data), "edit_field")) 
	addTableColumn( table, field );

    syncTable(table);

    /* for now assume this means they changed the table */
    /* is too aggressive but will be safe */
    mainWin = gtk_object_get_data(GTK_OBJECT(invoker), "filerwindow");
    mainWin->changed = TRUE;

    gtk_widget_destroy(data);
 }

static GtkWidget
*createColumnForm(gchar *title)
{
    GtkWidget *dialog;
    GtkWidget *table;
    GtkWidget *frame;
    GtkWidget *label;
    GtkWidget *entry;
    GtkWidget *yesbutton, *nobutton, *radiobox;
    GSList    *group;

    dialog = gnome_dialog_new (title,
			       GNOME_STOCK_BUTTON_OK,
			       GNOME_STOCK_BUTTON_CANCEL,
			       NULL);
    gtk_window_position(GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);

    frame = gtk_frame_new("Field Attributes");
    gtk_container_border_width(GTK_CONTAINER(frame), GNOME_PAD);
    gtk_box_pack_start (GTK_BOX(GNOME_DIALOG(dialog)->vbox), frame,
			FALSE, FALSE, 0);

    table = gtk_table_new(3, 3, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD);
    gtk_table_set_row_spacings(GTK_TABLE(table), 4);
    
    gnome_dialog_button_connect (GNOME_DIALOG(dialog), 0,
			GTK_SIGNAL_FUNC(fieldOkButtonCB),
			dialog);
    gnome_dialog_button_connect (GNOME_DIALOG(dialog), 1,
			GTK_SIGNAL_FUNC(fieldCancelButtonCB),
			dialog);

    label = gtk_label_new("Field");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		     GTK_FILL, GTK_FILL, 2, 2);
    entry = gtk_entry_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "field_name", entry);
    gtk_table_attach(GTK_TABLE(table), entry, 
		     1, 2, 0, 1,
		     GTK_FILL, GTK_FILL, 2, 2);

    label = gtk_label_new("Comment");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
		     GTK_FILL, GTK_FILL, 2, 2);
    entry = gtk_entry_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "field_comment", entry);
    gtk_table_attach(GTK_TABLE(table), entry, 
		     1, 2, 1, 2,
		     GTK_FILL, GTK_FILL, 2, 2);

    /* not null */
    label = gtk_label_new("Required?");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
		     GTK_FILL, GTK_FILL, 2, 2);
#if 0
    entry = gtk_toggle_button_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "field_required", entry);
    gtk_table_attach(GTK_TABLE(table), entry, 
		     1, 2, 2, 3,
		     GTK_EXPAND, GTK_EXPAND, 2, 2);
#else
    radiobox = gtk_hbox_new(FALSE, GNOME_PAD);
    yesbutton = gtk_radio_button_new_with_label(NULL, "Yes");
    group = gtk_radio_button_group(GTK_RADIO_BUTTON(yesbutton));
    nobutton = gtk_radio_button_new_with_label(group, "No");
    gtk_box_pack_start(GTK_BOX(radiobox), yesbutton, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(radiobox), nobutton, FALSE, FALSE, 0);
    gtk_object_set_data(GTK_OBJECT(dialog), "field_required_y", yesbutton);
    gtk_object_set_data(GTK_OBJECT(dialog), "field_required_n", nobutton);
    gtk_table_attach(GTK_TABLE(table), radiobox, 
		     1, 2, 2, 3,
		     GTK_EXPAND, GTK_EXPAND, 2, 2);

#endif

    /* create type selector with a default here */
    entry = createTypeSelector();
    gtk_box_pack_start (GTK_BOX(GNOME_DIALOG(dialog)->vbox), entry,
			FALSE, FALSE, 0);
    gtk_object_set_data(GTK_OBJECT(dialog), "field_type", entry);

    gtk_widget_show_all (dialog);
    return dialog;
}

/* called when user clicks 'Add' in the column editor */
static void
addColumnCB(GtkWidget *widget, gpointer data)
{
    GtkWidget  *fieldWidget;

    fieldWidget = createColumnForm("Add a Column");

    /*
     * Prime the dialog with a bit of state info
     */
    gtk_object_set_data (GTK_OBJECT (fieldWidget), "invoker_widget", data);
    gtk_object_set_data (GTK_OBJECT (fieldWidget), "edit_field", 
			 (gboolean *)FALSE);
}

/* called when user clicks wants to edit a column in the column editor */
/* accepts the clist widget containing column list as argument         */
static void
doEditColumn(GtkWidget *widget, GtkWidget *invoker)
{
    GtkWidget  *temp;
    GtkWidget  *fieldWidget;
    GList      *dlist;

    DBColumn   field;
    gchar      *val;

    gint       row;

    dlist = GTK_CLIST (widget)->selection;
    if (dlist) {
	row = (gint) dlist->data;
	field = (DBColumn)gtk_clist_get_row_data (GTK_CLIST(widget), row);

	fieldWidget = createColumnForm("Edit a Column");
	gtk_object_set_data (GTK_OBJECT (fieldWidget), "edit_field", 
			     (gboolean *)TRUE);
	gtk_object_set_data (GTK_OBJECT (fieldWidget), "invoker_widget",
			     invoker);

	temp = GTK_WIDGET (getWidget (fieldWidget, "field_name"));
	val  = getColumnName(field);
	gtk_entry_set_text (GTK_ENTRY (temp), (val) ? val : "");

	temp = GTK_WIDGET (getWidget (fieldWidget, "field_comment"));
	val  = getColumnComment(field);
	gtk_entry_set_text (GTK_ENTRY (temp), (val) ? val : "");

#if 0
	temp = GTK_WIDGET (getWidget (fieldWidget, "field_required"));
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(temp),
				    getColumnRequiredFlag(field));
#else
	if (getColumnRequiredFlag(field)) 
	    temp = GTK_WIDGET (getWidget (fieldWidget, "field_required_y"));
	else
	    temp = GTK_WIDGET (getWidget (fieldWidget, "field_required_n"));

	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(temp), TRUE);
#endif

	temp = GTK_WIDGET(getWidget(fieldWidget, "field_type"));
	if (getColumnDataType(field)->type != DATA_UNSET)
	    setTypeSelectorType(temp, getColumnDataType(field));
			  
	gtk_object_set_data(GTK_OBJECT(fieldWidget), "field_def", 
			     (DBColumn) field);
    }
}

static void
EditButtonSelectedCB(GtkWidget *widget, gpointer data)
{
    GtkWidget *temp;

    temp = (GtkWidget *)getWidget (data, "field_list");
    doEditColumn(temp, data);
}

/* called when user clicks 'Delete' in the column editor */
static void
deleteColumnCB(GtkWidget *widget, gpointer data)
{
    GtkWidget   *temp;
    GList       *dlist;
    DBColumn    field;
    DBTable     table;
    FilerWindow mainWin;
    gint        row;

    temp = (GtkWidget *)getWidget (data, "field_list");
    dlist = GTK_CLIST (temp)->selection;
    table = (DBTable)gtk_object_get_data (GTK_OBJECT (data), "table_def");
    if (dlist) {
	row = (gint)dlist->data;
	field = (DBColumn) gtk_clist_get_row_data(GTK_CLIST(temp), row);
	gtk_clist_unselect_row(GTK_CLIST(temp), row, 0);
	gtk_clist_remove(GTK_CLIST(temp), row);
	gtk_clist_select_row(GTK_CLIST(temp), (row > 0) ? row-1 : 0, 0);
	delTableColumn( table, getColumnName(field));

	/* mark the table as changed */
	mainWin = gtk_object_get_data(GTK_OBJECT(data), "filerwindow");
	mainWin->changed = TRUE;
    }
}

static gchar 
**createColumnEditWindowRow(DBColumn column)
{
    gchar **text;
    DataTypeSpec spec;

    text = (gchar **) g_malloc(6*sizeof(gchar *));
    spec=getColumnDataType(column);
    text[0] = getColumnName(column);
    text[1] = getColumnComment(column);
    text[2] = convertDataTypeSpecToString(spec);
    text[3] = (getColumnRequiredFlag(column)) ? "Y" : "N";
    text[4] = "";
    text[5] = NULL;

    return text;
}

/* handle double clicks on a column entry, run 'edit' on it */
static void
ColumnRowSelectedCB(GtkWidget *clist, gint row, gint col, 
		    GdkEventButton *event, gpointer data )
{
    if (event && event->type == GDK_2BUTTON_PRESS)
	doEditColumn(clist, (GtkWidget *)data);
}


/* given a table structure (which can be new and contain no columns) */
/* create a edit window                                              */
GtkWidget
*createColumnEditWindow(void)
{
    GtkWidget *window;
    GtkWidget *frame2, *frame3;
    GtkWidget *hbuttonbox1;
    GtkWidget *button;
    GtkWidget *clist;
    
    gchar *text[5] = { "Field Name", "Field Comment", "DataType", 
		       "Required?", "Default Value" };

    window = gtk_vbox_new(FALSE, 0);
/*    gtk_widget_set_usize(window, 200, -1); */
    
    frame2 = gtk_frame_new("Fields");
    gtk_frame_set_label_align(GTK_FRAME(frame2), 0.5, 0.5);
    gtk_container_border_width(GTK_CONTAINER(frame2), GNOME_PAD);

    /* create the clist containing all the fields */
    clist = gtk_clist_new_with_titles(5, text);
    gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_BROWSE);
    /*    gtk_clist_set_policy (GTK_CLIST(clist), GTK_POLICY_AUTOMATIC,
	  GTK_POLICY_AUTOMATIC);*/
    gtk_clist_set_border(GTK_CLIST(clist), GTK_SHADOW_IN);
    gtk_signal_connect(GTK_OBJECT(clist), "select_row",
		       GTK_SIGNAL_FUNC(ColumnRowSelectedCB),
		       window);

    /* setup each column individually */
    /* col 0 - field name */
    gtk_clist_set_column_width(GTK_CLIST(clist), 0, 100);
    gtk_clist_set_column_justification(GTK_CLIST(clist), 0, GTK_JUSTIFY_LEFT);

    /* col 1 - field comment */
    gtk_clist_set_column_width(GTK_CLIST(clist), 1, 150);
    gtk_clist_set_column_justification(GTK_CLIST(clist), 1, GTK_JUSTIFY_LEFT);

    /* col 2 - data type */
    gtk_clist_set_column_width(GTK_CLIST(clist), 2, 70);
    gtk_clist_set_column_justification(GTK_CLIST(clist), 2,GTK_JUSTIFY_CENTER);
    
    /* col 3 - Can be null? */
    gtk_clist_set_column_width(GTK_CLIST(clist), 3, 60);
    gtk_clist_set_column_justification(GTK_CLIST(clist), 3,GTK_JUSTIFY_CENTER);
    
    /* col 4 - Default Value */
    gtk_clist_set_column_width(GTK_CLIST(clist), 4, 100);
    gtk_clist_set_column_justification(GTK_CLIST(clist), 4,GTK_JUSTIFY_CENTER);

    /* store pointer to clist in our master widget for use in callbacks */
    gtk_object_set_data (GTK_OBJECT (window), "field_list", clist);

    gtk_container_add(GTK_CONTAINER(frame2), clist);
    gtk_box_pack_start(GTK_BOX(window), frame2, TRUE, TRUE, 0);

    frame3 = gtk_frame_new(NULL);
    gtk_container_border_width(GTK_CONTAINER(frame3), GNOME_PAD);

    gtk_box_pack_start(GTK_BOX(window), frame3, FALSE, FALSE, 0);
    
    hbuttonbox1 = gtk_hbutton_box_new();
    gtk_container_add(GTK_CONTAINER(frame3), hbuttonbox1);
    gtk_container_border_width(GTK_CONTAINER(hbuttonbox1), GNOME_PAD);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox1), 
			      GTK_BUTTONBOX_SPREAD);
    gtk_button_box_set_child_size(GTK_BUTTON_BOX(hbuttonbox1), 20, -1); 
				       

    /* all buttons will be set insensitive until table is attached to coledit*/
    
    button = gtk_button_new_with_label("Add");
    gtk_widget_set_sensitive(button, FALSE);
    gtk_object_set_data(GTK_OBJECT(window), "add_button", button);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC(addColumnCB),
			window);
    gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);
    
    button = gtk_button_new_with_label("Delete");
    gtk_widget_set_sensitive(button, FALSE);
    gtk_object_set_data(GTK_OBJECT(window), "del_button", button);
    gtk_signal_connect(GTK_OBJECT (button), "clicked",
		       GTK_SIGNAL_FUNC(deleteColumnCB),
		       window);
    gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);
    
    button = gtk_button_new_with_label("Edit");
    gtk_widget_set_sensitive(button, FALSE);
    gtk_object_set_data(GTK_OBJECT(window), "edit_button", button);
    gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC(EditButtonSelectedCB),
			window);
    gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);
    
    gtk_object_set_data (GTK_OBJECT (window), "parent_grab", 
			 (gboolean *)FALSE);
    gtk_object_set_data (GTK_OBJECT (window), "invoker_widget",
			 GTK_WIDGET (window));

    gtk_widget_show_all (window);
    return window;
}


/* clist has several columns                                           */
/*  fieldname  fieldcomment datatype canbenull defaultval              */
void
updateColumnEditWindow(GtkWidget *widget)
{
    GtkWidget *clist, *button;
    DBColumn  column;
    DBTable   table;
    GList     *dlist;
    gint      sens;
    gint      row=-1;

    table = gtk_object_get_data(GTK_OBJECT(widget), "table_def");
    g_return_if_fail( table != NULL );

    clist = (GtkWidget *)gtk_object_get_data (GTK_OBJECT (widget), 
					      "field_list");
    g_return_if_fail( clist != NULL );

    /* save current selection */
    dlist = GTK_CLIST (clist)->selection;
    if (dlist && dlist->data)
	row = (gint) dlist->data;

    /* keep it from blinking */
    gtk_clist_clear(GTK_CLIST(clist));
    gtk_clist_freeze(GTK_CLIST(clist));

    /* walk over all columns, refresh clist display */
    column = nextTableColumn( table, NULL );
    while (column) {
	gchar **text;
	gint row;

	text = createColumnEditWindowRow(column);
	row = gtk_clist_append( GTK_CLIST(clist),  text );
	gtk_clist_set_row_data( GTK_CLIST(clist), row, column );
	g_free(text);
	column = nextTableColumn( table, column );
    }

    /* set selection back active */
    gtk_clist_select_row( GTK_CLIST(clist), row, 1);

    /* let our changes be seen */
    gtk_clist_thaw(GTK_CLIST(clist));

    /* set buttons sensitive which should be */
    button = gtk_object_get_data(GTK_OBJECT(widget), "add_button");
    gtk_widget_set_sensitive(button, TRUE);

    sens = (nextTableColumn( table, NULL ) != NULL);
    button = gtk_object_get_data(GTK_OBJECT(widget), "del_button");
    gtk_widget_set_sensitive(button, sens);
    button = gtk_object_get_data(GTK_OBJECT(widget), "edit_button");
    gtk_widget_set_sensitive(button, sens);
}


/* set the table for use by the column editor */
void
setColumnEditWindowData(GtkWidget *widget, DBTable table)
{
    gtk_object_set_data(GTK_OBJECT(widget), "table_def", table);
    updateColumnEditWindow( widget );
}
