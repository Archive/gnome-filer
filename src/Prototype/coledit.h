#ifndef GNOMEFILER_COLEDIT_H
#define GNOMEFILER_COLEDIT_H

#include <gnome.h>

/* these routines handle the GUI which allows the user to create */
/* and modifiy the columns in a given table                      */

enum _EditResult {EDIT_OK, EDIT_ABORT};
typedef enum _EditResult EditResult;

GtkWidget *createColumnEditWindow(void);
void updateColumnEditWindow(GtkWidget *w);
void setColumnEditWindowData(GtkWidget *w, DBTable table);

#endif
