/* DB front-end
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */
 
#include <stdio.h>
#include <glib.h>

#include "dbfe.h"


/* basic data type descriptor - this tells us about each SQL data type   */
/* and things like if it is even supported. Will grow as I think of more */
/* things to add to it                                                   */
struct _DataTypeRec {
    gboolean     implemented;
    gchar        *name;
};
typedef struct _DataTypeRec DataTypeRec;

DataTypeRec DataTypes[] = {
    {FALSE, "Boolean"},
    {FALSE, "Integer*2"},
    {TRUE, "Integer*4"},
    {TRUE, "Float*4"},
    {FALSE, "Float*8"},
    {FALSE, "Char"},
    {TRUE, "Char(N)"},
    {FALSE, "VarChar(N)"},
    {FALSE, "Date"},
    {FALSE, "Time"},
    {FALSE, "Interval"},
    {FALSE, "Timestamp"},
    {FALSE, "END OF RECORD"},
    {FALSE, "Invalid Type"},
    {FALSE, "Unset"},
};

/*static gint NumDataTypes = sizeof(DataTypes) / sizeof(DataTypeRec);*/

static void invokeTableDeltaCBs( DBTable table );

/* utility routines, just for debugging mostly */
void
printColumnInfo( DBColumn col )
{
    printf("\n");
    printf("Column name    is \"%s\"\n", getColumnName( col ));
    printf("Column <%s> comment is \"%s\"\n", 
	   getColumnName( col ),
	   getColumnComment( col ));
    printf("Column <%s> required is %s\n", 
	   getColumnName( col ),
	   (getColumnRequiredFlag( col )) ? "TRUE" : "FALSE");
    printf("Column <%s> default val is %p\n", 
	   getColumnName( col ),
	   getColumnDefaultVal( col ));

}
    
void
printTableInfo( DBTable table )
{
    DBColumn col;

    printf("\n");
    printf("Table name    is \"%s\"\n", getTableName( table ));
    printf("Table <%s> comment is \"%s\"\n", 
	   getTableName( table ),
	   getTableComment( table ));
    printf("Table <%s> contains %d column(s)\n",
	   getTableName( table ),
	   numTableColumn( table ));
    printf("Column Data:\n");
    col = nextTableColumn( table, NULL );
    while (col != NULL) {
	printColumnInfo( col );
	col = nextTableColumn( table, NULL );
    }

}

/* Table routines */

/* createTable - create a new table, initialize to sane state */
DBTable
createTable( void )
{
    DBTable  tab;

    tab = g_malloc(sizeof(*tab));
    tab->name = NULL;
    tab->comment = NULL;
    tab->columns = NULL;

    tab->deltaCBs = NULL;
    tab->dieCBs = NULL;

    return tab;
}

/* set the name of the table - string is duplicated into the DBTable struct */
void
setTableName( DBTable table, gchar *name )
{

    g_return_if_fail(table != NULL);

    if (table->name)
	g_free(table->name);

    table->name = (name) ? g_strdup(name) : NULL;

    invokeTableDeltaCBs( table );
}

/* get name of the table */
gchar *
getTableName( DBTable table )
{
    g_return_val_if_fail( table != NULL, NULL );

    return table->name;
}

/* set the verbose comment associated with a table, string dup'd into table */
void
setTableComment( DBTable table, gchar *comment )
{
    g_return_if_fail( table != NULL );

    if (table->comment)
	g_free(table->comment);

    table->comment = (comment) ? g_strdup( comment ) : NULL;

    invokeTableDeltaCBs( table );
}

/* get the verbose comment of a table */
gchar
*getTableComment( DBTable table )
{
    g_return_val_if_fail( table != NULL, NULL );

    return table->comment;
}

/* msf@redhatcom - deal with callbacks for DBTable when DBTable changes */
/* i'll be honest, never tried to code this particular problem before */
/* so if there is a better solution using GtkObject's, ref counting, etc */
/* i'm all ears                                                          */
gint
addTableDeltaCB( DBTable table, DBTableCBFunc func, gpointer data)
{
    static gint key=0;
    CBFuncRec   p;

    g_return_val_if_fail( table != NULL, -1 );
    g_return_val_if_fail( func != NULL, -1 );

    p = g_malloc(sizeof(*p));
    p->key = key++;
    p->func = func;
    p->table = table;
    p->data = data;
    table->deltaCBs = g_list_append( table->deltaCBs, p );
    return p->key;
}

void
delTableDeltaCB( DBTable table, gint key)
{
    GList       *ptr;

    g_return_if_fail( table != NULL );
    g_return_if_fail( key < 0 );

    ptr = table->deltaCBs;
    while (ptr) {
	if (ptr->data && ((CBFuncRec)ptr->data)->key == key) {
	    g_free((CBFuncRec)ptr->data);
	    g_list_remove_link(table->deltaCBs, ptr);
	    return;
	} else {
	    ptr = g_list_next(ptr);
	}
    }
}

static void
invokeTableDeltaCBs( DBTable table )
{
    GList    *ptr;

    g_return_if_fail( table != NULL );

    ptr = table->deltaCBs;
    while (ptr) {
	(*(((CBFuncRec)ptr->data)->func))(((CBFuncRec)ptr->data)->table, 
	((CBFuncRec)ptr->data)->data); 
	ptr = g_list_next(ptr);
    }
}

/* helper function - if you mess with insides of a DBTable directly */
/*                   better call this to make sure everything gets  */
/*                   notified that DBTable changed                  */
void
syncTable( DBTable table )
{
    invokeTableDeltaCBs( table );
}


/* this set of routines handles adding and removing columns from a table */

/* insert a new column into the table */
gint
addTableColumn( DBTable table, DBColumn col )
{
    g_return_val_if_fail( table != NULL, -1 );
    g_return_val_if_fail( col != NULL, -1 );

    table->columns = g_list_append( table->columns, col );

    invokeTableDeltaCBs( table );

    return 0;
}


static GList *
findTableColumn( DBTable table, gchar *name )
{
    GList *ptr;

    /* find the matching entry by name */
    ptr = table->columns;
    while (ptr) {
	if ((ptr->data) && ((DBColumn)ptr->data)->name) {
	    if (strcmp(name, (gchar *)((DBColumn)ptr->data)->name)) {
		ptr = g_list_next(ptr);
		continue;
	    } else {
		return ptr;
	    }
	} else {
	    g_warning("in delTableColumn, NULL data in column list");
	}
    }

    return NULL;
}

/* delete named column from table - case sensitive */
gint
delTableColumn( DBTable table, gchar *name )
{
    GList *ptr;

    g_return_val_if_fail( table != NULL, -1 );
    g_return_val_if_fail( name != NULL, -1 );

    ptr = findTableColumn( table, name );
    if (!ptr)
	return -1;

    /* remove data and link from list */
    if (ptr->data)
	g_free((DBColumn)ptr->data);
    
    table->columns = g_list_remove_link(table->columns, ptr);

    invokeTableDeltaCBs( table );

    return 0;
}
	    
DBColumn
getTableColumnByName( DBTable table, gchar *name )
{
    GList *ptr;

    g_return_val_if_fail( table != NULL, NULL );
    g_return_val_if_fail( name != NULL, NULL );

    ptr = findTableColumn( table, name );
    if (!ptr)
	return NULL;
    else
	return (DBColumn)ptr->data;
}    

/* returns DBColumn after the one pointed to by cur.  */
/* return NULL if no more availble                    */
/* pass cur=NULL to get pointer to FIRST column       */
DBColumn
nextTableColumn( DBTable table, DBColumn cur )
{
    GList *ptr;

    g_return_val_if_fail( table != NULL, NULL );

    if (table->columns == NULL)
	return NULL;

    if (cur && cur->name)
	ptr = findTableColumn( table, cur->name );
    else
	return (DBColumn)(table->columns->data);

    if (!ptr)
	return NULL;

    ptr = g_list_next(ptr);
    if (!ptr)
	return NULL;
    else
	return (DBColumn)ptr->data;
}

guint
numTableColumn( DBTable table )
{
    g_return_val_if_fail ( table != NULL, 0 );

    if (table->columns == NULL)
	return 0;

    return g_list_length( table->columns );
}
    
    
/* these routines handle columns themselves */
DBColumn
createColumn( void )
{
    DBColumn  col;

    col = g_malloc( sizeof(*col) );

    col->type = g_malloc(sizeof(*col->type));
    col->type->type = DATA_UNSET;
    col->type->spec = NULL;
    col->name = NULL;
    col->comment = NULL;
    col->required = FALSE;
    col->defaultval = NULL;

    return col;
}

void
setColumnName( DBColumn col, gchar *name )
{
    g_return_if_fail( col != NULL );

    if (col->name)
	g_free(col->name);

    col->name = (name) ? g_strdup(name) : NULL;
}

gchar
*getColumnName( DBColumn col )
{
    g_return_val_if_fail( col != NULL, NULL );

    return col->name;
}



void
setColumnComment( DBColumn col, gchar *comment )
{
    g_return_if_fail( col != NULL );

    if (col->comment)
	g_free(col->comment);

    col->comment = (comment) ? g_strdup(comment) : NULL;
}

gchar 
*getColumnComment( DBColumn col )
{
    g_return_val_if_fail( col != NULL, NULL );

    return col->comment;
}


void 
setColumnRequiredFlag( DBColumn col, gboolean notnull )
{
    g_return_if_fail( col != NULL );

    col->required = notnull;
}

gboolean 
getColumnRequiredFlag( DBColumn col )
{
    g_return_val_if_fail( col != NULL, FALSE );

    return col->required;
}

void
setColumnDefaultVal( DBColumn col, gpointer data )
{
    g_return_if_fail( col != NULL );

    col->defaultval = data;
}

gpointer
getColumnDefaultVal( DBColumn col )
{
    g_return_val_if_fail( col != NULL, NULL );

    return col->defaultval;
}

/* copies type spec passed by user */
void
setColumnDataType( DBColumn col, DataTypeSpec type )
{
    g_return_if_fail( col != NULL );

    if (col->type)
	g_free(col->type);

    col->type = g_malloc(sizeof(*col->type));
    memcpy(col->type, type, sizeof(*col->type));
}

DataTypeSpec
getColumnDataType( DBColumn col )
{
    g_return_val_if_fail( col != NULL, NULL);

    return col->type;
}


/* routines to work with fundamental data types */
gint
getNumDataTypes(void)
{
    gint i, num;

    for (i=0, num=0; i != DATA_LAST; i++)
	if (isDataTypeImplemented(i))
	    num++;

    return num;
}

/* returns DATA_INVALID if no datatypes available, otherwise index of first */
DataTypeEnum
getFirstDataType(void)
{
    gint i;

    for (i=0; i != DATA_LAST; i++)
	if (isDataTypeImplemented(i))
	    break;

    return (i != DATA_LAST) ? i : DATA_INVALID;
}

/* given a data type, return next supported datatype */
DataTypeEnum
getNextDataType(DataTypeEnum type)
{
    gint i;

    if (type >= DATA_LAST)
	return DATA_INVALID;

    for (i=type+1; i != DATA_LAST; i++)
	if (isDataTypeImplemented(i))
	    break;

    return (i != DATA_LAST) ? i : DATA_INVALID;
}

/* given supported data type, return a string descriptor of it */    
gchar *
getDataTypeName(DataTypeEnum type)
{
    if (type < DATA_LAST)
	if (isDataTypeImplemented(type))
	    return DataTypes[type].name;

    return NULL;
}

/* given supported data type spec, return a string descriptor of it */    
gchar *
convertDataTypeSpecToString(DataTypeSpec spec)
{
    DataTypeEnum type;
    GString      *str;
    gchar        *p;

    g_return_val_if_fail( spec != NULL, NULL );
    type = spec->type;

    if (!isDataTypeImplemented(type))
	return NULL;

    switch (type) {
      case DATA_CHARN:
	str = g_string_new("");
	g_string_sprintf(str, "Char(%d)", GPOINTER_TO_INT(spec->spec));
	p = g_strdup(str->str);
	g_string_free(str, TRUE);
	break;
      default:
	p = g_strdup(DataTypes[type].name);
	break;
    }
    return p;
}

/* given supported data type name , return a enum value for it */    
DataTypeEnum
getDataTypeFromName(gchar *name)
{
    DataTypeEnum  i;
    gchar         *dataname;

    for (i=0; i != DATA_LAST; i++) {
	dataname = getDataTypeName(i);
	if (dataname && !strcmp(name, dataname))
	    break;
    }

    return (i != DATA_LAST) ? i : DATA_INVALID;
}

gboolean
isDataTypeImplemented(DataTypeEnum type)
{
    if (type < DATA_LAST)
	if (DataTypes[type].implemented)
	    return TRUE;

    return FALSE;
}

/* return default DataTypeSpec for given type */
DataTypeSpec
getDataTypeDefaultSpec( DataTypeEnum type)
{
    DataTypeSpec spec;

    spec = g_malloc(sizeof(*spec));
    spec->type = type;
    switch (type) {
      case DATA_CHARN:
	  spec->spec = GINT_TO_POINTER(20); /* 20 char by default */
	  break;
      default:
	  spec->spec = NULL;
	  break;
    }

    return spec;
}

