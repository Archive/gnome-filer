#ifndef _GNOMEFILER_DBFE_H_
#define _GNOMEFILER_DBFE_H_

#include <glib.h>

/* Database builder for GNOME/GTK                      */
/* Copyright 1998 Red Hat Software & Michael Fulbright */
/* Distributed under the GNU Public License            */

/***********************************************************************/
/* Def's dealing with the fields in a database                        */
/***********************************************************************/

/* database base types - exact copies of SQL92 base types */

enum _DataTypeEnum { 
		DATA_BOOL, DATA_INT2, DATA_INT4, DATA_FLOAT4, DATA_FLOAT8, 
		DATA_CHAR, DATA_CHARN, DATA_VARCHARN,
		DATA_DATE, DATA_TIME, DATA_INTERVAL, DATA_TIMESTAMP,
		DATA_LAST, DATA_INVALID, DATA_UNSET
};
typedef enum _DataTypeEnum  DataTypeEnum;

struct _DataTypeSpec {
    DataTypeEnum     type;    /* data type */
    gpointer        *spec;    /* data type specific attributes */
};

typedef struct _DataTypeSpec *DataTypeSpec;

/* field (column) in the database table */
struct _DB_COLUMN {
    DataTypeSpec type;         /* data type contained in this column */
    gchar       *name;         /* SQL name for this column           */
    gchar       *comment;      /* Verbose description of this column */
    gboolean     required;     /* can this column contain null rows  */
    gpointer     defaultval;   /* if non-null, contains default value*/
};

typedef struct _DB_COLUMN *DBColumn;

/* define different SQL types as C types */
typedef  gint16      SQL_INT2;
typedef  gint32      SQL_INT4;
typedef  gfloat      SQL_FLOAT4;
typedef  gdouble     SQL_FLOAT8;
typedef  gchar       SQL_CHAR;

struct _SQL_FIXSTR {
    gint      len;
    gchar    *str;
};

typedef  struct _SQL_FIXSTR  *SQL_CHARN;

typedef  struct _SQL_FIXSTR  *SQL_VARCHARN;


/* Not sure how to handle SQL date/time types yet */

    
/***********************************************************************/
/* Def's for handling a SQL table composed of columns                  */
/***********************************************************************/

struct _DB_TABLE {
    gchar *name;      /* SQL server name of table  */
    gchar *comment;   /* description of this table */
    GList *columns;   /* the column descriptors of the table  */
    GList *deltaCBs;  /* list of callbacks for all functions interested */
                      /* in notification when data structure changes */
    GList *dieCBs;    /* same as above, except for when data structure dies */
};

typedef struct _DB_TABLE *DBTable;


/* callback function definition */
typedef void(*DBTableCBFunc) (DBTable table, gpointer data);
struct _CBFuncRec {
    gint           key;
    DBTable        table;
    DBTableCBFunc  func;
    gpointer       data;
};
typedef struct _CBFuncRec *CBFuncRec;

/***********************************************************************/
/* functions for dealing with these datatypes                          */
/***********************************************************************/

/* tables */
DBTable createTable( void );
void setTableName( DBTable table, gchar *name );
void setTableComment( DBTable table, gchar *comment );
gchar *getTableName( DBTable table );
gchar *getTableComment( DBTable table );
gint   addTableColumn( DBTable table, DBColumn col );
gint   delTableColumn( DBTable table, gchar *name );
/* int    updateTableColumn( DBTable table, DBColumn col ); not needed? */
guint    numTableColumn( DBTable table );
DBColumn getTableColumnByName( DBTable table, gchar *name );
DBColumn nextTableColumn( DBTable table, DBColumn cur );
gint     addTableDeltaCB( DBTable table, DBTableCBFunc func, gpointer data );
void     delTableDeltaCB( DBTable table, gint key );
void     syncTable( DBTable table );

/* columns */
DBColumn createColumn( void );
void setColumnName( DBColumn col, gchar *name );
void setColumnComment( DBColumn col, gchar *comment );
void setColumnRequiredFlag( DBColumn col, gboolean notnull );
void setColumnDefaultVal( DBColumn col, gpointer data );
void setColumnDataType( DBColumn col, DataTypeSpec type );
gchar   *getColumnName( DBColumn col );
gchar   *getColumnComment( DBColumn col );
gboolean getColumnRequiredFlag( DBColumn col );
gpointer getColumnDefaultVal( DBColumn col );
DataTypeSpec getColumnDataType( DBColumn col );

/* misc utility routines */
void printTableInfo( DBTable table );
void printColumnInfo( DBColumn column );

/* data type handling routines */
gint getNumDataTypes(void);
DataTypeEnum getFirstDataType(void);
DataTypeEnum getNextDataType(DataTypeEnum type);
gchar *getDataTypeName(DataTypeEnum type);
gchar *convertDataTypeSpecToString(DataTypeSpec spec);
DataTypeEnum getDataTypeFromName(gchar *name);
gboolean isDataTypeImplemented(DataTypeEnum type);
DataTypeSpec getDataTypeDefaultSpec(DataTypeEnum type);
#endif
