/* Edit columns in a given table
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */

#include <gnome.h>

#include "dbfe.h"
#include "coledit.h"
#include "filerwindow.h"
#include "formedit.h"
#include "tabedit.h"
#include "tableio.h"
#include "uimisc.h"


static void newTableCB(GtkWidget *widget, gpointer data);
static void loadTableCB(GtkWidget *widget, gpointer data);
static void saveTableCB(GtkWidget *widget, gpointer data);
static void loadLayoutCB(GtkWidget *widget, gpointer data);
static void saveLayoutCB(GtkWidget *widget, gpointer data);
static void configMainCB(GtkWidget *widget, gpointer data);
static void aboutMainCB(GtkWidget *widget, gpointer data);
static void quitMainCB(GtkWidget *widget, gpointer data);
static void YesNoQuestionCB(gint reply, gpointer data);


/* Menu and toolbar structures */

static GnomeUIInfo filemenu[] = {
        {GNOME_APP_UI_ITEM, 
         "New form", "Open a new form",
         newTableCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
         0, 0, NULL},
	GNOMEUIINFO_SEPARATOR,
        {GNOME_APP_UI_ITEM, 
         "Load Table", "Load Table from disk",
         loadTableCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
         0, 0, NULL},
        {GNOME_APP_UI_ITEM, 
         "Save Table", "Save Table to disk",
         saveTableCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
         0, 0, NULL},
	GNOMEUIINFO_SEPARATOR,
        {GNOME_APP_UI_ITEM, 
         "Load Layout", "Load Layout from disk",
         loadLayoutCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
         0, 0, NULL},
        {GNOME_APP_UI_ITEM, 
         "Save Layout", "Save Layout to disk",
         saveLayoutCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
         0, 0, NULL},
	GNOMEUIINFO_SEPARATOR,
        {GNOME_APP_UI_ITEM, 
         "Preferences","Preferences",
         configMainCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP,
         0, 0, NULL},
        {GNOME_APP_UI_ITEM, 
         "Exit", "Exit",
         quitMainCB, NULL, NULL, 
         GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT,
         0, 0, NULL},
        GNOMEUIINFO_END
};

static GnomeUIInfo helpmenu[] = {
    {GNOME_APP_UI_ITEM, 
     "About", "Info about this program",
     aboutMainCB, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
     0, 0, NULL},
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_HELP("gnome-filer"),
    GNOMEUIINFO_END
};

static GnomeUIInfo mainmenu[] = {
    GNOMEUIINFO_SUBTREE("File", filemenu),
    GNOMEUIINFO_SUBTREE("Help", helpmenu),
    GNOMEUIINFO_END
};


static void
deleteMainCB (GtkWidget *w, void *foo, gpointer data)
{
    gboolean lose;
    GtkWidget *dialog;

    dialog = gnome_question_dialog_modal( "Lose current changes?",
					  YesNoQuestionCB, &lose);
    gnome_dialog_run(GNOME_DIALOG(dialog));
    
    if (!lose) {
	g_message("Aborting quit");
	g_message("Should probably ask using to save and quit...");
	return;
    }

    gtk_main_quit();
}

/* user wants to create a new form */
static void
newTableCB(GtkWidget *widget, gpointer data)
{
    GtkWidget *tableform;

    g_message("newTableCB: should probably do something if table already"
	      " exists...");


    tableform = createTableForm(NULL);

    gtk_object_set_data (GTK_OBJECT (tableform), "invoker_filer", data);
}

static void
SaveTableFileSelectOkCB( GtkWidget *w, GtkFileSelection *fs )
{
    gchar *filename;
    DBTable table;

    filename = gtk_file_selection_get_filename(fs);

    table = gtk_object_get_data(GTK_OBJECT(fs), "tabledef");

    saveTableDefToFile(filename, table);

    gtk_widget_destroy(GTK_WIDGET(fs));
}

static void
LoadTableFileSelectOkCB( GtkWidget *w, GtkFileSelection *fs )
{
    FilerWindow mainWin;
    gchar *filename;
    DBTable table;

    filename = gtk_file_selection_get_filename(fs);

    mainWin = gtk_object_get_data(GTK_OBJECT(fs), "filerwindow");

    if (readTableDefFromFile(filename, &table)) {
	g_message("Error reading table def %s",filename);
	gtk_widget_destroy(GTK_WIDGET(fs));
	return;
    }

    setFilerWindowData(mainWin, table);
    gtk_widget_destroy(GTK_WIDGET(fs));

}


static void
saveTableCB(GtkWidget *widget, gpointer data)
{
    FilerWindow mainWin;
    GtkWidget *fileSelector;

    mainWin = (FilerWindow)data;
    if (mainWin->mode != FILERMODE_EDIT) {
	g_message("In saveTableCB and not in editting mode!");
	return;
    }

    if (!mainWin->table) {
	g_message("In saveTableCB we do not have a table!");
	return;
    }

    /* get filename from user */
    fileSelector = gtk_file_selection_new("Save Table Definition");
    gtk_object_set_data(GTK_OBJECT(fileSelector), "tabledef", mainWin->table);
#if 0
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel), PATH_MESSAGES); 
#endif
    gtk_window_position(GTK_WINDOW(fileSelector), GTK_WIN_POS_MOUSE);
    gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(fileSelector)->ok_button),
                        "clicked", (GtkSignalFunc) SaveTableFileSelectOkCB,
                        fileSelector);
    gtk_signal_connect_object(GTK_OBJECT(
                              GTK_FILE_SELECTION(fileSelector)->cancel_button),
			      "clicked", (GtkSignalFunc) gtk_widget_destroy,
			      (gpointer) fileSelector);
   
    gtk_widget_show_all(fileSelector);

    return;
}

/* handle responses to yes/no questions posed to user */
static void
YesNoQuestionCB(gint reply, gpointer data)
{
    gboolean   *yesno = (gboolean *) data;;

    *yesno = (reply == GNOME_YES);
}
    


static void
loadTableCB(GtkWidget *widget, gpointer data)
{
    FilerWindow mainWin;
    GtkWidget *fileSelector;
    GtkWidget *dialog;


/* MSF - fix this to ask if user wants to save old table IF it */
/*       has been changed - also need to go thru and mark doc  */
/*       as changed everywhere it can, and check if its changed */
/*       when we load/quit/etc                                  */



    mainWin = (FilerWindow)data;
    if (mainWin->mode != FILERMODE_EDIT) {
	g_message("In loadTableCB and not in editting mode!");
	return;
    }
    
    /* has the user made modifications? */
    if (mainWin->changed) {
	gboolean lose;
	
	dialog = gnome_question_dialog_modal( "Lose current changes?",
				 YesNoQuestionCB, &lose);
	gnome_dialog_run(GNOME_DIALOG(dialog));
	
	if (!lose)
	    return;
    }

#if 0
    dialog = gnome_dialog_new (title,
			       GNOME_STOCK_BUTTON_OK,
			       GNOME_STOCK_BUTTON_CANCEL,
			       NULL);
    gtk_window_position(GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
    
    frame = gtk_frame_new("Overwrite");
    gtk_container_border_width(GTK_CONTAINER(frame), GNOME_PAD);
    gtk_box_pack_start (GTK_BOX(GNOME_DIALOG(dialog)->vbox), frame,
			FALSE, FALSE, 0);
    
    table = gtk_table_new(3, 3, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD);
    gtk_table_set_row_spacings(GTK_TABLE(table), 4);
    
    gnome_dialog_button_connect (GNOME_DIALOG(dialog), 0,
				 GTK_SIGNAL_FUNC(fieldOkButtonCB),
				 dialog);
    gnome_dialog_button_connect (GNOME_DIALOG(dialog), 1,
				 GTK_SIGNAL_FUNC(fieldCancelButtonCB),
				 dialog);
#endif

    
    if (mainWin->table) {
	g_message("In loadTableCB we have a table - SHOULD DO SOMETHING LIKE TELL USER!");
    }
    
    /* get filename from user */
    fileSelector = gtk_file_selection_new("Save Table Definition");
    gtk_object_set_data(GTK_OBJECT(fileSelector), "filerwindow", 
			(gpointer) mainWin);
#if 0
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel), PATH_MESSAGES); 
#endif
    gtk_window_position(GTK_WINDOW(fileSelector), GTK_WIN_POS_MOUSE);
    gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(fileSelector)->ok_button),
		       "clicked", (GtkSignalFunc) LoadTableFileSelectOkCB,
		       fileSelector);
    gtk_signal_connect_object(GTK_OBJECT(
	GTK_FILE_SELECTION(fileSelector)->cancel_button),
			      "clicked", (GtkSignalFunc) gtk_widget_destroy,
			      (gpointer) fileSelector);
    
    gtk_widget_show_all(fileSelector);
    
}

static void
saveLayoutCB(GtkWidget *widget, gpointer data)
{

    g_message("save layout not implemented yes");
}

static void
loadLayoutCB(GtkWidget *widget, gpointer data)
{

    g_message("load layout not implemented yes");
}

static void
configMainCB(GtkWidget *widget, gpointer data)
{
    g_message("configMainCB not implemented");
}

static void
quitMainCB(GtkWidget *widget, gpointer data)
{
    gboolean lose;
    GtkWidget *dialog;
	
    dialog = gnome_question_dialog_modal( "Lose current changes?",
					  YesNoQuestionCB, &lose);
    gnome_dialog_run(GNOME_DIALOG(dialog));
    
    if (!lose) {
	g_message("Aborting quit");
	g_message("Should probably ask using to save and quit...");
	return;
    }

    g_message("quitMainCB called, no special cleanup done");
    gtk_main_quit();
}

static void
aboutMainCB(GtkWidget *widget, gpointer data)
{
    GtkWidget *about;
    const gchar *authors[] = {"Michael Fulbright   <msf@redhat.com>", NULL};

    about = gnome_about_new ( "Gnome Filer", "0.1",
			      "Copyright Red Hat Software (C) 1998",
			      authors,
			      NULL, 
			      NULL);

    gtk_widget_show (about);
}


FilerWindow
createFilerWindow(gchar *name, FilerWindowMode initialMode)
{

    FilerWindow w;
    GtkWidget   *widget;

    w = g_malloc(sizeof(*w));

    /* default to edit mode for now */
    if (initialMode != FILERMODE_EDIT) {
	g_message("createFilerWindow - only FILERMODE_EDIT supported");
	return NULL;
    }

    w->mode = initialMode;

    /* create main window and menus/toolbar first */
    w->app = gnome_app_new(name, "Gnome Filer");
    gtk_widget_set_usize(w->app, 550, 350);
    gtk_signal_connect(GTK_OBJECT(w->app), "delete_event",
		       GTK_SIGNAL_FUNC(deleteMainCB),
		       w);
    gtk_window_set_wmclass (GTK_WINDOW (w->app), "GnomeFiler",
			    "GnomeFiler");

    gnome_app_create_menus_with_data(GNOME_APP(w->app), mainmenu, w);

    /* make panes needed for the different GUI components of a filer window */
    w->vpaned = gtk_vpaned_new();

    gnome_app_set_contents(GNOME_APP(w->app), w->vpaned);
    
    /* now create the frame for the form view/editor */
    w->upperPane   = gtk_hbox_new(FALSE, 0);
    gtk_paned_add1(GTK_PANED(w->vpaned), w->upperPane);
    w->lowerPane   = gtk_hbox_new(FALSE, 0);
    gtk_paned_add2(GTK_PANED(w->vpaned), w->lowerPane);
    

    if (w->mode == FILERMODE_EDIT) {
	/* create the form/field editors */
	widget = createFormEditWindow();
	gtk_container_add(GTK_CONTAINER(w->upperPane), widget);
	gtk_object_set_data(GTK_OBJECT(widget), "filerwindow",
			    (gpointer) w);
	w->formEditor = widget;

	widget = createColumnEditWindow();
	gtk_container_add(GTK_CONTAINER(w->lowerPane), widget);
	gtk_object_set_data(GTK_OBJECT(widget), "filerwindow",
			    (gpointer) w);
	w->fieldEditor = widget;

    } else if (w->mode == FILERMODE_QUERY) {
	w->formEditor = NULL;
	w->fieldEditor = NULL;
    } else {
	g_message("Error - unknown filer mode!");
    }

    /* no table yet */
    w->table   = NULL;
    w->changed = FALSE;

    /* all done show it */
    gtk_widget_show_all(w->app);

    return w;
}


/* redraw stuff when table changes */
static void
handleTableChangeCB( DBTable table, gpointer data )
{
    FilerWindow w;

    w = (FilerWindow) data;
    updateColumnEditWindow( w->fieldEditor );
    updateFormEditWindow( w->formEditor );
}


/* given a filerwindow, set the table for the form editor and field editor */
void
setFilerWindowData(FilerWindow w, DBTable table)
{
    /* assume we've already asked user about losing previous table */
    w->table = table;
    w->changed = FALSE; 

    setColumnEditWindowData( w->fieldEditor, table);
    setFormEditWindowData( w->formEditor , table);

    gtk_window_set_title (GTK_WINDOW (w->app), getTableName(table));

    /* set this so that everyone will get notified if user changes something */
    /* and we can redraw windows appropriately                               */
    addTableDeltaCB( table, handleTableChangeCB, w );
}
