#ifndef GNOMEFILER_FILERWINDOW_H
#define GNOMEFILER_FILERWINDOW_H

#include "dbfe.h"

enum _FilerWindowMode {FILERMODE_EDIT, FILERMODE_QUERY};
typedef enum _FilerWindowMode FilerWindowMode;


/* these describe the main window */
struct _FilerWindow {
    FilerWindowMode mode;       /* edit/query/etc */
    GtkWidget     *app;         /* main GnomeApp window */

#if 0
    GtkWidget     *formFrame;   /* frame containing the entry form */
    GtkWidget     *fieldFrame;  /* frame contained field list/editor */
    GtkWidget     *resultFrame; /* frame containing result(s) of query */
#endif

    GtkWidget     *formEditor;  /* actual parent widget of form editor */
    GtkWidget     *fieldEditor; /*   "      "      "    "  field editor */
    GtkWidget     *resultList;  /*   "      "      "    "  result list  */

    GtkWidget     *upperPane;   /* container for the top of vpaned */
    GtkWidget     *lowerPane;   /* container for the bottom of vpaned */
    GtkWidget     *vpaned;      /* pane for top/bottom */
                                /* top is the form and field frames */
                                /* bottom is the query results list */
    
    DBTable       table;        /* table we're working with */
    gboolean      changed;      /* has user changed the form? */
};

typedef struct _FilerWindow *FilerWindow;


FilerWindow createFilerWindow( gchar *name, FilerWindowMode initialMode );
void setFilerWindowData( FilerWindow w, DBTable table );


#endif
