/* 
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */


#include <gnome.h>

#include "dbfe.h"

/* create a window which we can edit a form for a given table */
GtkWidget
*createFormEditWindow(void)
{

    GtkWidget *window;
    GtkWidget *frame;
    GtkWidget *canvas;
    GtkWidget *table;
    GtkWidget *w;

    window = gtk_vbox_new(FALSE, 0);

    table = gtk_table_new (2, 2, FALSE);
    gtk_table_set_row_spacings (GTK_TABLE (table), 4);
    gtk_table_set_col_spacings (GTK_TABLE (table), 4);
    gtk_container_add(GTK_CONTAINER(window), table);

    frame = gtk_frame_new("Form Editor");
    gtk_frame_set_label_align(GTK_FRAME(frame), 0.5, 0.5);
    gtk_container_border_width(GTK_CONTAINER(frame), 2);

    gtk_table_attach (GTK_TABLE (table), frame,
		      0, 1, 0, 1,
		      GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		      GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		      0, 0);
  
    canvas = gnome_canvas_new();
    gtk_widget_pop_visual();
    gtk_widget_pop_colormap();

    gtk_widget_set_usize (canvas, 100, 100);
    gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 0, 0, 100, 100);
    gtk_container_add(GTK_CONTAINER(frame), canvas);
    gtk_object_set_data(GTK_OBJECT(window), "form_canvas", canvas);
    gtk_object_set_data(GTK_OBJECT(window), "form_items", NULL);

    w = gtk_hscrollbar_new (GTK_LAYOUT (canvas)->hadjustment);
    gtk_table_attach (GTK_TABLE (table), w,
		      0, 1, 1, 2,
		      GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		      GTK_FILL,
		      0, 0);
    
    
    w = gtk_vscrollbar_new (GTK_LAYOUT (canvas)->vadjustment);
    gtk_table_attach (GTK_TABLE (table), w,
		      1, 2, 0, 1,
		      GTK_FILL,
		      GTK_EXPAND | GTK_FILL | GTK_SHRINK,
		      0, 0);

    gtk_widget_show_all (window);
    return window;
}

/* draws a representation of current table */
void
updateFormEditWindow( GtkWidget *widget )
{
    DBTable  table;
    DBColumn column;
    GtkWidget *canvas;
    GnomeCanvasGroup *oldgroup;
    GnomeCanvasItem  *group;
    GList     *items;
    gint      x, y;
    gchar     *name;

    table = gtk_object_get_data(GTK_OBJECT(widget), "table_def");
    g_return_if_fail( table != NULL );

    canvas = (GtkWidget *)gtk_object_get_data(GTK_OBJECT (widget),
					       "form_canvas");
    g_return_if_fail( canvas != NULL );

    /* see if any items exist */
    items = gtk_object_get_data(GTK_OBJECT(widget), "form_items");
    while (items) {
	oldgroup = GNOME_CANVAS_GROUP(items->data);
	printf("Destroying canvas group %s\n",
	       (gchar *)gtk_object_get_data(GTK_OBJECT(oldgroup),"item_name"));
	gtk_object_destroy(GTK_OBJECT(oldgroup));
	items = g_list_next(items);
    }
    g_list_free(items);

    items = NULL;
    if (items == NULL) {
	/* build canvas up from scratch */

	column = nextTableColumn( table, NULL );
	x = 15;
	y = 5;
	while (column) {
	    name = getColumnName(column);
	    printf("Creating label for field %s\n",name);

	    group = gnome_canvas_item_new (GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas)->root),
					  gnome_canvas_group_get_type (),
					  "x", (gdouble) x, "y", (gdouble) (y),
					  NULL);
 
	    gnome_canvas_item_new (GNOME_CANVAS_GROUP (group),
				   gnome_canvas_rect_get_type (),
				   "x1", 150.0,
				   "y1", 0.0,
				   "x2",  275.0,
				   "y2",  20.0,
				   "fill_color", "red",
				   "outline_color", "black",
				   "width_pixels", 0,
				   NULL);
	    
	    gnome_canvas_item_new (GNOME_CANVAS_GROUP (group),
				   gnome_canvas_text_get_type (),
				   "text", name,
				   "x", (double) 0,
				   "y", (double) 8.5,
				   "font", "-adobe-helvetica-bold-r-normal--12-*-75-75-*-*-*-*",
				   "anchor", GTK_ANCHOR_WEST,
				   "fill_color", "black",
				   NULL);
	    gtk_object_set_data(GTK_OBJECT(group), "item_name", name);
	    items = g_list_append( items, group );
	    
	    column = nextTableColumn( table, column );
	    y += 25;
	}
        gtk_object_set_data(GTK_OBJECT(widget), "form_items", items);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 0, 0, 100, y+25);
    } else {
	/* update only canvas items of interest */
	g_message("canvas update not supported (yet)");
    }
}



/* use to associate a DBTable with a given formeditor */
void 
setFormEditWindowData(GtkWidget *widget, DBTable table)
{
    /* for now we create label for each field in table */
    gtk_object_set_data(GTK_OBJECT(widget), "table_def", table);

    updateFormEditWindow( widget );
}
    
