#ifndef GNOMEFILER_FORMEDIT_H
#define GNOMEFILER_FORMEDIT_H

#include <gnome.h>

GtkWidget *createFormEditWindow(void);
void updateFormEditWindow(GtkWidget *w);
void setFormEditWindowData(GtkWidget *w, DBTable table);

#endif
