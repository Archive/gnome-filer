/* 
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */


#include <gnome.h>

#include "filerwindow.h"
#include "dbfe.h"


void
table_changed_cd( DBTable table, gpointer data ) {
    g_message("Table changed, data is %p\n", data);
}


gint
main(gint argc, gchar *argv[])
{

    FilerWindow w;

    DBTable table;
    DBColumn col;
    DataTypeSpec spec;

    gnome_init("gnome-filer", NULL, argc, argv);

    /* setup a test table to play with */
    table = createTable();
    addTableDeltaCB( table, table_changed_cd, NULL );
    setTableName(table, "Gnome Users");
    setTableComment(table, "the smart people");

    col = createColumn();
    spec = g_malloc(sizeof(*spec));
    spec->type = DATA_CHARN;
    spec->spec = GINT_TO_POINTER(10);
    setColumnName( col, "Name" );
    setColumnComment( col, "who they are" );
    setColumnDataType( col, spec );
    setColumnRequiredFlag(col, TRUE);
    addTableColumn( table, col );

    col = createColumn();
    spec->spec = GINT_TO_POINTER(30);
    setColumnName( col, "Address" );
    setColumnComment( col, "where they are" );
    setColumnDataType( col, spec );
    addTableColumn( table, col );

    col = createColumn();
    spec->spec = GINT_TO_POINTER(15);
    setColumnName( col, "Job" );
    setColumnComment( col, "why they are" );
    setColumnDataType( col, spec );
    addTableColumn( table, col );

    /* now display filerwindow */
    w = createFilerWindow("test", FILERMODE_EDIT);
/*    setFilerWindowData(w, table);   */

    gtk_main();

    return 0;
}
