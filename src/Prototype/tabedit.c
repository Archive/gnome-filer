/* 
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */

#include <gnome.h>

#include "dbfe.h"
#include "filerwindow.h"
#include "uimisc.h"
#include "tabedit.h"

static void tableOkButtonCB(GtkWidget *widget, gpointer data);
static void tableCancelButtonCB(GtkWidget *widget, gpointer data);

static void
tableOkButtonCB(GtkWidget *widget, gpointer data)
{
    GtkWidget  *temp;
    FilerWindow  filer;
    DBTable    table;

    table = createTable();

    filer = (FilerWindow)gtk_object_get_data (data, "invoker_filer");
    
    temp = (GtkWidget *)getWidget (data, "table_name");
    printf("Table name -> %s\n",gtk_entry_get_text (GTK_ENTRY (temp)));
    setTableName(table, gtk_entry_get_text (GTK_ENTRY (temp)));
    temp = (GtkWidget *)getWidget (data, "table_comment");
    printf("Table comment -> %s\n", gtk_entry_get_text (GTK_ENTRY (temp)));
    setTableComment(table, gtk_entry_get_text (GTK_ENTRY (temp)));

    g_message("setting table for the filerwindow to new table, didnt check "
	      "if table already existed");

    setFilerWindowData(filer, table);
    
    gtk_widget_destroy (data);
}

static void
tableCancelButtonCB(GtkWidget *widget, gpointer data)
{
    gtk_widget_destroy(data);
}


GtkWidget
*createTableForm(gchar *title)
{
    GtkWidget *dialog;
    GtkWidget *table1;
    GtkWidget *frame;
    GtkWidget *label;
    GtkWidget *entry;
    
    dialog = gnome_dialog_new ((title) ? title : "New Table",
			       GNOME_STOCK_BUTTON_OK,
			       GNOME_STOCK_BUTTON_CANCEL,
			       NULL);

    frame = gtk_hbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dialog)->vbox), frame);
    gtk_container_border_width(GTK_CONTAINER(frame), 2);

    table1 = gtk_table_new(3, 2, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table1);
    gtk_container_border_width(GTK_CONTAINER(table1), 4);
    gtk_table_set_row_spacings(GTK_TABLE(table1), 4);
    
    gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0,
			GTK_SIGNAL_FUNC(tableOkButtonCB),
			dialog);
    gnome_dialog_button_connect (GNOME_DIALOG(dialog), 1,
			GTK_SIGNAL_FUNC(tableCancelButtonCB),
			dialog);

    label = gtk_label_new("Table Name:");
    gtk_table_attach(GTK_TABLE(table1), label, 0, 1, 0, 1,
		     GTK_FILL, GTK_FILL, 0, 0);
    entry = gtk_entry_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "table_name", entry);
    gtk_table_attach(GTK_TABLE(table1), entry, 
		     1, 2, 0, 1,
		     GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_label_new("Comment:");
    gtk_table_attach(GTK_TABLE(table1), label, 0, 1, 1, 2,
		     GTK_FILL, GTK_FILL, 0, 0);
    entry = gtk_entry_new();
    gtk_object_set_data(GTK_OBJECT(dialog), "table_comment", entry);
    gtk_table_attach(GTK_TABLE(table1), entry, 
		     1, 2, 1, 2,
		     GTK_FILL, GTK_FILL, 0, 0);

    gtk_widget_show_all(dialog);
    return dialog;
}


