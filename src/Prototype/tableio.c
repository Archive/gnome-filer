/* handles file i/o for reading and writing table descriptions 
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <gnome.h>
#include <errno.h>

#include "dbfe.h"

/* Format is simple at the moment (XML in the future I hope?) */
/*                                                            */
/* table {                                                    */
/*    name = <table name>                                     */
/*    comment = <table name>                                  */
/*                                                            */
/*    column {                                                */
/*        name = <column name>                                */
/*        comment = <column comment>                          */
/*        required = <column required flag>                   */
/*        default  = <column default value> (depend on type)  */
/*        datatype {                                          */
/*            type = <datatype enum>                          */
/*            <datatype specific key/value pairs follow>      */
/*        }                                                   */
/*    }                                                       */
/* }                                                          */

/* some gscanner code from Owen Taylor - thanks Owen! */

enum {
  TOKEN_INVALID = G_TOKEN_LAST,
  TOKEN_TABLE,
  TOKEN_COLUMN,
  TOKEN_DATATYPE,
  TOKEN_NAME,
  TOKEN_COMMENT,
  TOKEN_REQUIRED,
  TOKEN_DEFAULT,
  TOKEN_TYPE,
  TOKEN_LENGTH,
};

enum {
  PARSE_OK,
  PARSE_ERROR,
  PARSE_SYNTAX,
  PARSE_DONE
};

static struct
{
  gchar *name;
  gint token;
} symbols[] = {
  { "table", TOKEN_TABLE },
  { "column", TOKEN_COLUMN },
  { "datatype", TOKEN_DATATYPE },
  { "name", TOKEN_NAME },
  { "comment", TOKEN_COMMENT },
  { "required", TOKEN_REQUIRED },
  { "default", TOKEN_DEFAULT },
  { "type", TOKEN_TYPE },
  { "length", TOKEN_LENGTH }
};
static guint nsymbols = sizeof (symbols) / sizeof (symbols[0]);

/* parse the datatype { } record into the given column */
/* FALSE return value means success                    */
static gboolean
parseDataTypeIntoColumn( DBColumn col, GScanner *scanner )
{
    GTokenType token;
    DataTypeSpec type;

    /* expect first entry to be 'type = <datatype>' */
    /* anything else is an error                    */
    token = g_scanner_get_next_token (scanner);
    if (token != TOKEN_TYPE)
	return TRUE;
    
    token = g_scanner_get_next_token (scanner);
    if (token != G_TOKEN_EQUAL_SIGN)
	return TRUE;
    
    token = g_scanner_get_next_token (scanner);
    if (token != G_TOKEN_STRING)
	return TRUE;

    type = g_malloc(sizeof(*type));
    type->type=getDataTypeFromName(scanner->value.v_string);
    if (type->type == DATA_INVALID)
	return TRUE;
    
    switch (type->type) {
      case DATA_CHARN:
	/* read in max length for fixed length string */
	token = g_scanner_get_next_token (scanner);
	if (token != TOKEN_LENGTH)
	    return TRUE;
	
	token = g_scanner_get_next_token (scanner);
	if (token != G_TOKEN_EQUAL_SIGN)
	    return TRUE;
	
	token = g_scanner_get_next_token (scanner);
	if (token != G_TOKEN_INT)
	    return TRUE;
	
	type->spec = GINT_TO_POINTER(scanner->value.v_int);
	break;
	
      default:
	type->spec = NULL;
	break;
    }
    
    /* should have closing token */
    token = g_scanner_get_next_token (scanner);
    if (token != G_TOKEN_RIGHT_CURLY)
	return TRUE;
    
    /* store data type spec in the column */
    setColumnDataType( col, type );

    return FALSE;
}

static gboolean
parseColumnIntoTable( DBTable table, GScanner *scanner )
{
    GTokenType token;
    DBColumn col;
    
    token = g_scanner_get_next_token (scanner);
    if (token != G_TOKEN_LEFT_CURLY)
	return TRUE;
    
    col = createColumn();
    
    /* column section accepts:                                 */
    /* 'name' 'comment' 'required' 'default' 'datatype'        */
    while (1) {
	GTokenType field_token = g_scanner_get_next_token (scanner);
	GTokenType token;
	
	if (field_token == G_TOKEN_RIGHT_CURLY) 
	    break;

	token = g_scanner_get_next_token (scanner);

	if (token == G_TOKEN_EQUAL_SIGN) {
	    switch (field_token) {
	      case TOKEN_NAME:
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    return TRUE;
		setColumnName(col, scanner->value.v_string);
		break;
		
	      case TOKEN_COMMENT:
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    return TRUE;
		setColumnComment(col, scanner->value.v_string);
		break;
		
	      case TOKEN_REQUIRED:
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    return TRUE;
		setColumnRequiredFlag(col, *scanner->value.v_string == 'Y');
		break;
		
	      case TOKEN_DEFAULT:
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    return TRUE;
		g_message("Ignoring default line in tabledef");
		break;

	      default:
		return TRUE;
		break;
	    } 
	} else if (token == G_TOKEN_LEFT_CURLY) {
	    switch (field_token) {
	      case TOKEN_DATATYPE:
		if (parseDataTypeIntoColumn(col, scanner))
		    return TRUE;
		break;

	      default:
		return TRUE;
		break;
	    }
	} else {
	    return TRUE;
	}
    }

    addTableColumn(table, col);
    return FALSE;
}

gboolean
readTableDefFromFile (gchar *filename, DBTable *table)
{
    gint i;
    gint fd;
    GScanner *scanner;
    GTokenType token;
    
    DBTable newTable=NULL;

    *table = NULL;
    fd = open (filename, O_RDONLY);
    if (!fd) {
	g_warning ("Can't open %s: %s\n", filename, g_strerror(errno));
	return TRUE;
    }
    
    scanner = g_scanner_new (NULL);
    scanner->config->symbol_2_token = TRUE;
    
    for (i = 0; i < nsymbols; i++)
	g_scanner_add_symbol (scanner, symbols[i].name, 
			      GINT_TO_POINTER (symbols[i].token));
    
    g_scanner_input_file (scanner, fd);
    scanner->input_name = filename;
    while (1) {
	token = g_scanner_get_next_token (scanner);
	if (token == G_TOKEN_EOF) {
	    goto finished;
	}
	
	/* expect 'table' to be first thing in file */
	if (token != TOKEN_TABLE)
	    goto error;
	
	/* create new table */
	newTable = createTable();
	
	token = g_scanner_get_next_token (scanner);
	if (token != G_TOKEN_LEFT_CURLY)
	    goto error;
	
	while (1)	{
	    GTokenType field_token = g_scanner_get_next_token (scanner);
	    if (field_token == G_TOKEN_RIGHT_CURLY)
		break;
	    
	    /* table section accepts tokens 'name' and 'column' */
	    if (field_token == TOKEN_NAME) {
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_EQUAL_SIGN)
		    goto error;
		
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    goto error;
		
		setTableName(newTable, scanner->value.v_string);
		if (strlen(getTableName(newTable)) == 0)
		    goto error;
	    } else if (field_token == TOKEN_COMMENT) {
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_EQUAL_SIGN)
		    goto error;
		
		token = g_scanner_get_next_token (scanner);
		if (token != G_TOKEN_STRING)
		    goto error;
		
		setTableComment(newTable, scanner->value.v_string);
		if (strlen(getTableComment(newTable)) == 0)
		    goto error;
	    } else if (field_token == TOKEN_COLUMN) {
		if (parseColumnIntoTable(newTable, scanner))
		    goto error;
	    } else {
		goto error;
	    }
	}
    }
    
 error:
    g_scanner_error (scanner, "Error parsing file\n");
    return TRUE;

 finished:
    g_scanner_destroy (scanner);
    close (fd);

    *table = newTable;
    return FALSE;
}


/* utility routines, just for debugging mostly */
static void
saveColumnDefToFile( DBColumn col, gint fd )
{
    GString *str1, *str2;
    DataTypeSpec  spec;

    /* start with the column section intro */
    str1 = g_string_new("    column {\n");

    /* add the column attributes */
    str2 = g_string_new("");
    g_string_sprintf(str2,
		      "        name = \"%s\"\n"
		      "        comment = \"%s\"\n"
		      "        default = \"%s\"\n"
		      "        required = '%c'\n"
		      "        datatype {\n",
		      getColumnName(col),
		      getColumnComment(col),
#if 0
		      getColumnDefaultVal(col), 
#else
		      "",
#endif
		      (getColumnRequiredFlag(col) ? 'Y' : 'N'));
    g_string_append(str1, str2->str);
    g_string_free(str2, TRUE);

    /* finally, tack on the datatype definition  */
    spec = getColumnDataType(col);
    str2 = g_string_new("");
    g_string_sprintf(str2, "            type = \"%s\"\n",
		    getDataTypeName(spec->type));
    g_string_append(str1, str2->str);
    g_string_free(str2, TRUE);

    /* any data type specific tags */
    switch (spec->type) {
      case DATA_CHARN:
	str2 = g_string_new("");
	g_string_sprintf(str2,"            length = %d\n",
			 GPOINTER_TO_INT(spec->spec));
	g_string_append(str1, str2->str);
	g_string_free(str2, TRUE);
	break;
      default:
	break;
    }
    g_free(spec);
    g_string_append(str1, "        }\n    }\n\n");
    write(fd, str1->str, strlen(str1->str));
    return;
}

/* given a DBTable, save it to the specified disk file        */
/* the resulting file can be loaded with loadTableDefFromDisk */
/*                                                            */
gboolean
saveTableDefToFile(gchar *filename, DBTable table )
{
    gint fd;
    GString  *buf;
    DBColumn col;

    g_return_val_if_fail( table != NULL, TRUE );
    g_return_val_if_fail( filename != NULL, TRUE );

    fd = open (filename, O_WRONLY|O_CREAT, 0600);
    if (!fd) {
	g_warning ("Can't open %s: %s\n", filename, g_strerror(errno));
	return TRUE;
    }

    /* start by outputting table name and comment */
    buf = g_string_new("");
    g_string_sprintf(buf, "table {\n"
	     "    name = \"%s\"\n"
	     "    comment = \"%s\"\n\n", 
	     getTableName(table), getTableComment(table));
    write(fd, buf->str, strlen(buf->str));
    g_string_free(buf, TRUE);

    /* now do columns */
    col = nextTableColumn( table, NULL );
    while (col != NULL) {
	saveColumnDefToFile( col, fd );
	col = nextTableColumn( table, col );
    }

    buf = g_string_new("}\n");
    write(fd, buf->str, strlen(buf->str));
    g_string_free(buf, TRUE);
    
    close(fd);
    return FALSE;    
}
