#ifndef GNOMEFILER_TABLEIO_H
#define GNOMEFILER_TABLEIO_H

/* handles file i/o for reading and writing table descriptions */

gboolean readTableDefFromFile (gchar *filename, DBTable *table);
gboolean saveTableDefToFile(gchar *filename, DBTable table);



#endif
