/* test for DB front-end routines
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */


#include <stdio.h>

#include "dbfe.h"
#include "coledit.h"



void
delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
    gtk_main_quit ();
}


int
main( int argc, char **argv)
{
    GtkWidget *editor;
    GtkWidget *app;

    DBTable   table;
    DBColumn  col;

    gchar      *name, *comment;

    gnome_init("test-dbfe", NULL, argc, argv, 0, NULL); 

    app = gnome_app_new("test-dbfe", "Test");

    printf("Testing columns now\n");

    col = createColumn();
    setColumnName( col, "Field1" );
    setColumnComment( col, "Test field for test-dbfe" );

    printColumnInfo( col );

    setColumnDefaultVal( col, GINT_TO_POINTER(2) );

    printColumnInfo( col );

    setColumnRequiredFlag( col, TRUE );

    printColumnInfo( col );

    printf("\n\nTesting Tables Now\n\n");
    table = createTable();
    setTableName(table, "Table1");
    setTableComment(table, "Test table for test-dbfe");

    printTableInfo( table );

    printf("Result of adding a table column was %d\n",
	   addTableColumn( table, col ));

    printTableInfo( table );

    col = createColumn();
    setColumnName( col, "Field2" );
    setColumnComment( col, "Another test field for test-dbfe" );
    addTableColumn( table, col );

    editor = createColumnEditWindow();
    setColumnEditWindowData(editor, table);

    gnome_app_set_contents(GNOME_APP(app), editor);

    gtk_signal_connect (GTK_OBJECT(editor), "delete_event",
			GTK_SIGNAL_FUNC(delete_event), NULL);
    gtk_widget_show(app);

    gtk_main();

    return 0;
}

	   
