/* misc functions with no other home
 *
 * Copyright (C) 1998 Red Hat Software
 *
 * Author: Michael Fulbright <msf@redhat.com>
 */
#include <gnome.h>

#include "uimisc.h"

GtkWidget *
getWidget (GtkWidget  *widget, const gchar *name)
{
    GtkWidget *found;

/* seems to work better this way... */    
/*    if (widget->parent)
	widget = gtk_widget_get_toplevel (widget);
*/
    found = gtk_object_get_data (GTK_OBJECT (widget), name);
    if (!found) {
        g_warning ("Widget not found: %s", name);
        return NULL;
    }
    return found;
}
