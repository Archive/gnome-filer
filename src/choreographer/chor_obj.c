/*
 * Choreographer RTL header file
 *
 * Defines a "Choreographer Object" data type; depends on GLIB
 *
 * Copyright 1999  by David Orme <orme@cs.columbia.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include "chor_obj.h"

ChorObj *chor_init(int argc, char *argv[])
{
}

ChorObj *chor_obj_clone(ChorObj *obj)
{
}

void chor_obj_add_member(gchar *name, ChorType type, gsize size)P
{
}

gpointer chor_obj_get(ChorObj *obj, gchar *name)
{
}

void chor_obj_put(ChorObj *obj, gchar *name, gpointer value)
{
}

