/*
 * Choreographer RTL header file
 *
 * Defines a "Choreographer Object" data type; depends on GLIB
 *
 * Copyright 1999  by David Orme <orme@cs.columbia.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Choreographer object model documentation
 *
 * Naming Conventions for Choreographer stuff:
 *   Start all stuff with chor_ (the name is a pun on "core")
 *
 * Choreographer are defined dynamically at runtime.  The RTTI is
 * defined completely in run time data structures, making it possible
 * to serialize a Choreographer object without the object's consent or
 * help.
 *
 * Choreographer objects are conceptually structs, but internally are
 * accessed like arrays that can contain multiple data types.  ie:
 * Once you know a field's offset from the start of the object, you
 * can use the object pointer as an array of pointers, then cast the
 * result to the desired data type.  The translation from field name
 * to field offset is wrapped in a function.
 *
 * Choreographer objects do not contain methods or signals.  Instead,
 * they define "watchpoints" on their member data according to the
 * Observer pattern.  When the watched data changes, all observers are
 * notified of the change through a callback function.  Note that it
 * is possible to simulate signals by creating data members whose only
 * purpose is to "be watched."  Another way to get methods is to add
 * data members that happen to be function pointers.  Subclasses can
 * "override" these methods by replacing the function pointers.
*/


#ifndef __CHOR_OBJ_H__
#define __CHOR_OBJ_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* We depend on glib */
#include <glib.h>

  /* Define Choreographer types here */
  typedef struct _ChorClass ChorClass;
  typedef struct _ChorMember ChorMember;
  typedef enum _ChorType ChorType;
  typedef gpointer ChorObj;

  /* Constructor and destructor function callback types */
  typedef ChorObj (*ChorConstCB)();
  typedef void (*ChorDestCB)(ChorObj dying);
  

  /* This lists the data types Choreographer's objects support */
  enum _ChorType {CHOR_BUILTIN, 
		  CHOR_REF};


  /* A Choreographer class  */
  struct _ChorClass
  {
    ChorConstCB	*constructor;	/* The class's constructor function */
    ChorDestCB	*destructor;	/* The class's destructor function */
    gchar	*name;		/* The class's name */
    GList	*members;	/* The list of ChorMembers */
    gsize	size;		/* Size of an instance in memory */
  };

  /* Choreographer object's data member */
  struct _ChorMember
  {
    gchar	*name;		/* The name of the data member */
    gulong	offset;		/* Offset from the beginning of the
				   struct in bytes */
    GSList	*watchlist;	/* For when we implement watches */
    ChorType	literalOrRef;	/* Is this a literal or a refrence */
  };


  /*
   * Methods here...
   */

  /* Initialize the Chor library and return the base object */
  ChorObj chor_init(int argc, char *argv[]);

  /* Make a new ChorObj and add elements to it */
  ChorObj *chor_obj_clone(ChorObj *obj);
  void chor_obj_add_member(gchar *name, ChorType type, gsize size);
  
  /* Get / put operations */
  gpointer chor_obj_get(ChorObj *obj, gchar *name);
  void chor_obj_put(ChorObj *obj, gchar *name, gpointer value);

  /* Get with ID and Put with ID */
  /* type chor_obj_get_member(ChorObj obj, gulong member_id, datatype type);*/
  #define CHOR_OBJ_GET_MEMBER(obj, member_id, member_datatype)	\
    ( ((member_datatype[])(obj.obj))[offset/sizeof(member_datatype)] )
  

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CHOR_OBJ_H */


