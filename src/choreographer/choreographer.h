/*
 * Main Choreographer RTL header file
 *
 * Copyright 1999  by David Orme <orme@cs.columbia.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __CHOR_OBJ_H__
#define __CHOR_OBJ_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#include "chor_obj.h"


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CHOR_OBJ_H */


