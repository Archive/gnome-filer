/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Event handler
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>
#include "events.h"

static void events_motion_cb        (GtkWidget *widget, GdkEventMotion *event);
static void events_mouse_down_cb    (GtkWidget *widget, GdkEventButton *event);
static void events_mouse_up_cb      (GtkWidget *widget, GdkEventButton *event);
static void events_mouse_clicked_cb (GtkWidget *button, void *data);
static void events_size_allocate_cb (GtkWidget *widget,
				     GtkAllocation *allocation,
				     gpointer data);

void
events_register_widget (GtkWidget *widget)
{
	guint signal_id;
	
	if (!GTK_IS_WIDGET(widget))
		return;

	if  (!GTK_WIDGET_NO_WINDOW (widget))
	{
		gtk_widget_set_events (widget,
				       GDK_ENTER_NOTIFY_MASK |
				       GDK_LEAVE_NOTIFY_MASK |
				       GDK_MOTION_NOTIFY |
				       GDK_POINTER_MOTION_HINT_MASK |
				       GDK_BUTTON_MOTION_MASK |
				       GDK_BUTTON_PRESS_MASK);
	}

	gtk_signal_connect (GTK_OBJECT (widget),
			    "motion_notify_event",
			    (GtkSignalFunc) events_motion_cb,
			    NULL);
	gtk_signal_connect (GTK_OBJECT (widget),
			    "button_press_event",
			    (GtkSignalFunc) events_mouse_down_cb,
			    NULL);
	gtk_signal_connect (GTK_OBJECT (widget),
			    "button_release_event",
			    (GtkSignalFunc) events_mouse_up_cb,
			    NULL);
	gtk_signal_connect (GTK_OBJECT (widget),
			    "size-allocate",
			    (GtkSignalFunc) events_size_allocate_cb,
			    NULL);
	
	signal_id = gtk_signal_lookup ("clicked",
				       GTK_OBJECT_TYPE (GTK_OBJECT (widget)));
	if (signal_id >=1)
	{
		gtk_signal_connect (GTK_OBJECT (widget), "clicked",
				    GTK_SIGNAL_FUNC (events_mouse_clicked_cb),
				    NULL);
	}
	
}

static void
events_motion_cb (GtkWidget *widget, GdkEventMotion *event)
{
	if (events_callbacks.mouse_motion_cb)
	{
		events_callbacks.mouse_motion_cb (widget, event);
	}
}

static void 
events_mouse_down_cb (GtkWidget *widget, GdkEventButton *event)
{
	if (events_callbacks.mouse_down_cb)
	{
		events_callbacks.mouse_down_cb (widget, event);
	}
}

static void 
events_mouse_up_cb (GtkWidget *widget, GdkEventButton *event)
{
	if (events_callbacks.mouse_up_cb)
	{
		events_callbacks.mouse_up_cb (widget, event);
	}
}

static void
events_mouse_clicked_cb (GtkWidget *button, void *data)
{
	if (events_callbacks.mouse_clicked_cb)
	{
		events_callbacks.mouse_clicked_cb(button, data);
	}
	data_set (GTK_OBJECT (button), "clicked", 1);
}

static void
events_size_allocate_cb (GtkWidget *widget,
			 GtkAllocation *allocation,
			 gpointer data)
{
	data_set (GTK_OBJECT (widget), "X", 
		  (gpointer) allocation->x);
	data_set (GTK_OBJECT (widget), "Y", 
		  (gpointer) allocation->y);
	data_set (GTK_OBJECT (widget), "WIDTH", 
		  (gpointer) allocation->width);
	data_set (GTK_OBJECT (widget), "HEIGHT", 
		  (gpointer) allocation->height);
}
