/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Event handler
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GNOME_FILER_EVENTS_H__
#define __GNOME_FILER_EVENTS_H__


struct {
	void (*mouse_motion_cb)  (GtkWidget *widget, GdkEventMotion *event);
	void (*mouse_down_cb)    (GtkWidget *widget, GdkEventMotion *event);
	void (*mouse_up_cb)      (GtkWidget *widget, GdkEventMotion *event);
	void (*mouse_clicked_cb) (GtkWidget *widget, GdkEventMotion *event);
} events_callbacks;

void events_register_widget (GtkWidget *widget);


#endif  /* __GNOME_FILER_EVENTS_H__ */
