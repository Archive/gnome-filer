/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Framework (top level window) definition
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gnome.h>
#include <watchpoint.h>
#include "object.h"

#include "framework.h"
#include "menu-file.h"
#include "db-objects.h"
#include "interfaces.h"

static void set_canvas_view (GtkObject *, gchar *, gpointer);

void
framework_init ( void )
{
	GtkWidget *app;
	GtkWidget *cardpanel;
	GtkWidget *workspace;
	GtkWidget *cardspace;
	GtkWidget *hpaned;
	GtkWidget *test_button;
	GtkObject *db_canvas;
	GtkObject *db_tree;
	GtkObject *interfaces_canvas;
	GtkObject *interfaces_tree;

	app = object_new_from_class ("Application");
	data_set (GTK_OBJECT (app), "name", (gpointer) "gnome-filer");
	data_set (GTK_OBJECT (app), "label", (gpointer) "Gnome Filer");
	data_set (GTK_OBJECT (app), "menu", "def");

	cardpanel   = object_new_from_class ("Cardpanel");
	hpaned      = object_new_from_class ("Hpaned");
	test_button = object_new_from_class ("Button");
	workspace   = object_new_from_class ("Vbox");
	cardspace   = object_new_from_class ("Fixed");

	db_canvas = db_objects_new_canvas();
	db_tree   = db_objects_tree_from_canvas(db_canvas);
	gtk_object_set_data (GTK_OBJECT (db_tree), "canvas", db_canvas);

	interfaces_canvas = interfaces_new_canvas();
	interfaces_tree   = interfaces_tree_from_canvas(interfaces_canvas);
	gtk_object_set_data (GTK_OBJECT (interfaces_tree), "canvas", interfaces_canvas);

	data_set (GTK_OBJECT (cardpanel), "index", (gpointer) 1);
	data_set (GTK_OBJECT (cardpanel), "label", (gpointer) "DB Objects");
	if (db_tree)
	{
		data_set (GTK_OBJECT (cardpanel), "child", (gpointer) db_tree);
	}

	data_set (GTK_OBJECT (cardpanel), "index", (gpointer) 2);
	data_set (GTK_OBJECT (cardpanel), "label", (gpointer) "Watchpoints");
	data_set (GTK_OBJECT (cardpanel), "index", (gpointer) 3);
	data_set (GTK_OBJECT (cardpanel), "label", (gpointer) "Procedures");
	data_set (GTK_OBJECT (cardpanel), "child", (gpointer) test_button);

	data_set (GTK_OBJECT (cardpanel), "index", (gpointer) 4);
	data_set (GTK_OBJECT (cardpanel), "label", (gpointer) "Interfaces");
	if (interfaces_tree)
	{
		data_set (GTK_OBJECT (cardpanel), "child", (gpointer) interfaces_tree);
	}

	data_set_watchpoint (GTK_OBJECT (cardpanel),
			     "open_card",
			     set_canvas_view,
			     (gpointer) hpaned);

/*
	data_set (cardspace, "child", (gpointer) cardpanel);
*/
	data_set (GTK_OBJECT (hpaned), "left_child", (gpointer) cardpanel);

	data_set (GTK_OBJECT (app), "new_child", (gpointer) hpaned);
	data_set (GTK_OBJECT (hpaned), "divider", (gpointer) 150);

	/*
	 * Connect up the menu callbacks
	*/
	data_set_watchpoint (GTK_OBJECT (app), "/File/New doc", 
			     menu_file_new_cb, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "/File/Open...", 
			     menu_file_open_cb, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "/File/Save As...", 
			     menu_file_save_as_cb, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "/File/Save", 
			     menu_file_save_cb, NULL);
	data_set_watchpoint (GTK_OBJECT (app), "/File/Exit", 
			     menu_file_exit_cb, NULL);

	gtk_widget_show_all (app);
	data_set (GTK_OBJECT (cardpanel), "select", (gpointer) 4);
}



static void
set_canvas_view (GtkObject *cardpanel, gchar *key, gpointer data)
{
	GtkObject *child;
	GtkObject *canvas;
	GtkObject *hpaned = GTK_OBJECT (data);

	g_return_if_fail (cardpanel != NULL);
	g_return_if_fail (hpaned != NULL);
	g_return_if_fail (key != NULL);

	child = data_get (cardpanel, "child");
	if (child && hpaned)
	{
		canvas = (GtkObject *) gtk_object_get_data (child, "canvas");
		if (canvas)
		{
			data_set (hpaned, "right_child", (gpointer) canvas);
		}
	}
}
