/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * interfaces.c: the interfaces builder
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/


#include <config.h>
#include <gnome.h>
#include <watchpoint.h>
#include "object.h"
#include "interfaces.h"

static void item_select (GtkObject *item, gchar *key, gpointer data);

GtkObject *
interfaces_new_canvas (void)
{
	GtkObject *canvas;
	GtkObject *canvas_text;
	GtkObject *canvas_tree;

	canvas      = object_new_from_class ("Canvas");
	canvas_text = object_new_from_class ("Text");
	canvas_tree = object_new_from_class ("Tree");

	data_set (canvas_text, "x", (gpointer) 50);
	data_set (canvas_text, "y", (gpointer) 0);
	data_set (canvas_text, "label", "Interfaces");
	data_set (canvas, "child", (gpointer) canvas_text);

	data_set (canvas, "tree", (gpointer) canvas_tree);

	data_set (canvas_tree, "path", "/interfaces");

	return canvas;
}


GtkObject *
interfaces_tree_from_canvas (GtkObject *canvas)
{
	return (GtkObject *) data_get (canvas, "tree");
}


static void
item_select (GtkObject *item,
	     gchar *key,
	     gpointer data)
{
	gboolean selected;

	selected = (gboolean) data_get (item, key);

	if (selected)
	{
		g_print ("Item Selected\n");
	}
	else
	{
		g_print ("Item Deselected\n");
	}
}
