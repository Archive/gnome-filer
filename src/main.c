/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Main file, startup code.
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gnome.h>
#include <watchpoint.h>

#include "framework.h"
#include "plugins.h"

static void
gf_main (int argc, char *argv [])
{
	/*
	 *  Initialize the plug-ins first,
	 *  as we use plug-in objects in all
	 *  aspects of Gnome Filer
	*/
	gnome_init ("gnome-filer", "0.1", argc, argv);
	plugins_init ();
	framework_init ();
	
	
	gtk_main ();
	
}

int
main (int argc, char *argv [])
{
	gf_main (argc, argv);
	return 0;
}
