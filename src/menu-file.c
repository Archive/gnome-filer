/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Menu: File menu callbacks
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>
#include "menu-file.h"

void 
menu_file_new_cb (GtkObject *app, gchar *key, gpointer data)
{
	g_print ("In new....\n");
}

void
menu_file_save_cb (GtkObject *app, gchar *key, gpointer data)
{

}


void
menu_file_save_as_cb (GtkObject *app, gchar *key, gpointer data)
{

}


void
menu_file_open_cb (GtkObject *app, gchar *key, gpointer data)
{
	g_print ("In open....\n");

}


void
menu_file_exit_cb (GtkObject *app, gchar *key, gpointer data)
{
	data_set (app, "destroy", 1);
}
