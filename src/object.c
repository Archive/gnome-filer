/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Object definitions
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>
#include "plugins.h"
#include "object.h"
#include "watchpoint.h"
#include "events.h"


/*
 * NAME:    object_new_from_class
 *
 * ARGS:    gchar *class -- a string indicating the class of 
 *                         control to create.
 *
 * RETURNS: GtkWidget *widget -- The new object.
 *
 * PURPOSE: Pretty obvious-- create an object with default values.
 *
 * NOTES:   This should be used to create a brand-spankin'-new object.
 *          Use object_new_from_data to create a previously-defined
 *          control.
 *
 *          This calls events_register_widget (from events.c) to connect 
 *          up all the proper event signals, and data_register_widget
 *          (from data.c) to set up the basic structures for watchpoints.
 *          (SEE /SRCDIR/doc/watch.txt for more info on watchpoints.)
*/

GtkWidget *
object_new_from_class (gchar *class)
{
	GtkWidget          *widget = NULL;
	PluginControl      *pc = NULL;
	ObjectControl      *oc = NULL;
	
	if (class) 
	{
		pc = plugins_lookup (class);
		if (pc && pc->type == PLUGIN_OBJECT)
			oc = (ObjectControl *) pc->extended_info;
	}
	
	if (oc && oc->new_instance)
	{
		widget = oc->new_instance ();
		
		if (widget)
		{
			events_register_widget (widget);
		}
		else
		{
			g_warning ("Couldn't create control of class %s\n", class);
		}
	}
	else
	{
		g_warning ("Couldn't find control of class %s\n", class);
	}

	return widget;
}
