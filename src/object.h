/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Object definitions
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GNOME_FILER_OBJECT_H__
#define __GNOME_FILER_OBJECT_H__

#include <glib.h>

/* Some common definitions */

#define OBJECT_VERSION_LEVEL  1

typedef enum   _ObjectType ObjectType;
typedef struct _ObjectControl ObjectControl;

enum _ObjectType {
	OBJECT_UNKNOWN,
	OBJECT_TOPLEVEL,
	OBJECT_CONTAINER,
	OBJECT_CONTROL,
	OBJECT_DBA
};


/*
 *  ObjectControl:
 *
 *  library contains the library handle
 *  PluginType is obvious
 *
 *  extended info will contain the real plugin info,
 *  which depends on the type of plugin.  For instance,
 *  and object will put a pointer to an ObjectPlugin
 *  structure.
 *
 *  library is filled by the plugin loader
 *  The rest is filled in by the plugin initialization
 *  routine.
*/


struct _ObjectControl {
	ObjectType  object_type;
	gint        version;
	GtkObject  *(*new_instance) ( void );
};


GtkWidget *object_new_from_class (gchar *class);


#endif  /* __GNOME_FILER_OBJECT_H__ */
