/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Plugin definition
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <sys/stat.h>
#include <sys/dir.h>
#include <stdio.h>

#include <config.h>
#include <gnome.h>
#include <watchpoint.h>
#include "plugins.h"


static GHashTable *plugins_hash = NULL;


static void plugins_load_plugin ( char *dirname, struct direct *plugin );
static void plugins_load_from_dir (char *dirname);


void
plugins_init ( void )
{
	gchar *fullname;
	
	fullname = g_malloc0 (1024);
	
	if (!plugins_hash)
		plugins_hash = g_hash_table_new (g_str_hash, g_str_equal);

	g_snprintf (fullname, 1024, "%s/%s", SHAREDDIR,
		    "share/gnome-filer/controls");
	plugins_load_from_dir (fullname);
	g_free (fullname);
}


PluginControl *
plugins_lookup (gchar *class)
{
	return (PluginControl *) g_hash_table_lookup (plugins_hash, class);
}


/*
 * NAME:    plugins_load_from_dir
 *
 * ARGS:    char *dirname -- a string containing a directory 
 *                           in which to search for modules.
 *
 * RETURNS: N/A
 *
 * PURPOSE: Loads modules from the specified directory.
 *
 * NOTES:   Uses the g_module routines to locate, load, and
 *          initialize the various modules.
 *
 *          Modules, or plug-ins, are dynamic library files
 *          that provide the necessary functionality for all
 *          the widgets.  Each widget resides in its own module
 *          file.
*/

static void
plugins_load_from_dir (char *dirname)
{
	struct stat sbuf;
	
	if (stat (dirname, &sbuf) < 0)
	{
		g_warning ("Can't find module directory %s\n", dirname);
	}
	else if ((sbuf.st_mode & S_IFMT) == S_IFDIR)
	{
		DIR *dp;
		struct direct *plugin;

		if ((dp = opendir(dirname)) == NULL)
		{
			g_warning ("Can't open directory %s\n", dirname);
			return;
		}

		while ((plugin = readdir(dp)) != NULL)
		{
			if (plugin->d_ino == 0) 
			{
				continue;
			}
			
			stat (plugin->d_name, &sbuf);
			
			if (!(( (sbuf.st_mode & S_IFMT) != S_IFREG) ||
			      ( (sbuf.st_mode & S_IFMT) != S_IFIFO)))
			{
				continue;
			}

			plugins_load_plugin (dirname, plugin);
		}
		closedir (dp);
	}
}



static void
plugins_load_plugin ( char *dirname, struct direct *plugin )
{
	GModule *library;
	gchar fullname[1024];
	PluginControl *pc;

	g_snprintf (fullname, 1024, "%s/%s", dirname, plugin->d_name);
	library = g_module_open (fullname, 0);

	if (!library)
	{
		g_warning ("Error: %s \n", g_module_error());
		return;
	}

	pc = (PluginControl *) g_malloc0 (sizeof(PluginControl));
	if (!g_module_symbol (library, "plugin_class_init",
			      (gpointer) &pc->plugin_class_init))
	{
		g_warning ("%s is not a valid control.\n", fullname);
		g_free (pc);
		return;
	}

	g_print ("Loading %s\n", fullname);
	pc->plugin_class_init(pc);

	if (!g_hash_table_lookup (plugins_hash, pc->classname) &&
	    pc->version && pc->version <= PLUGIN_VERSION_LEVEL)
	{
		g_hash_table_insert (plugins_hash, pc->classname, pc);
	}
	else if (pc)
	{
		if (pc->plugin_class_cleanup)
		{
			pc->plugin_class_cleanup ();
		}
		g_free (pc);
	}
}

