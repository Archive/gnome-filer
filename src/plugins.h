/*
 * Gnome-Filer, a database front-end RAD tool
 *
 * Plugin definitions
 *
 * Author:
 *   Anthony Taylor (tony@searhc.org)
 *
*/

/*
 * Copyright 2000 by Anthony Taylor & David Orme
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GNOME_FILER_PLUGINS_H__
#define __GNOME_FILER_PLUGINS_H__

#include <glib.h>
#include <gmodule.h>

/* Some common definitions */

#define PLUGIN_VERSION_LEVEL  1

typedef enum _PluginType PluginType;
typedef struct _PluginControl PluginControl;

enum _PluginType {
	PLUGIN_UNKNOWN,
	PLUGIN_OBJECT,
	PLUGIN_LANGUAGE
};


/*
 *  PluginControl:
 *
 *  library contains the library handle
 *  PluginType is obvious
 *
 *  extended info will contain the real plugin info,
 *  which depends on the type of plugin.  For instance,
 *  and object will put a pointer to an ObjectPlugin
 *  structure.
 *
 *  library is filled by the plugin loader
 *  The rest is filled in by the plugin initialization
 *  routine.
*/


struct _PluginControl {
	GModule    *library;
	void      (*plugin_class_init)    (PluginControl *);
	void      (*plugin_class_cleanup) ( void );	
	PluginType  type;
	gchar      *classname;
	gint        version;
	gpointer    extended_info;
};


/* Function Prototypes */

void           plugins_init ( void );
PluginControl *plugins_lookup (gchar *class);

#endif  /* __GNOME_FILER_PLUGINS_H__ */
